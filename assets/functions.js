/*
 * Detact Mobile Browser
 */
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('html').addClass('ismobile');
}

$(window).on('load', function () {
    /* --------------------------------------------------------
     Page Loader
     -----------------------------------------------------------*/
    if (!$('html').hasClass('ismobile')) {
        if ($('.page-loader')[0]) {
            setTimeout(function () {
                $('.page-loader').fadeOut();
            }, 500);

        }
    }
})

$(document).ready(function () {

    /* --------------------------------------------------------
     Layout
     -----------------------------------------------------------*/
    (function () {

        //Get saved layout type from LocalStorage
        var layoutStatus = localStorage.getItem('ma-layout-status');

        if (!$('#header-2')[0]) {  //Make it work only on normal headers
            if (layoutStatus == 1) {
                $('body').addClass('sw-toggled');
                $('#tw-switch').prop('checked', true);
            }
        }

        $('body').on('change', '#toggle-width input:checkbox', function () {
            if ($(this).is(':checked')) {
                setTimeout(function () {
                    $('body').addClass('toggled sw-toggled');
                    localStorage.setItem('ma-layout-status', 1);
                }, 250);
            } else {
                setTimeout(function () {
                    $('body').removeClass('toggled sw-toggled');
                    localStorage.setItem('ma-layout-status', 0);
                }, 250);
            }
        });
    })();

    /* --------------------------------------------------------
     Scrollbar
     -----------------------------------------------------------*/
    function scrollBar(selector, theme, mousewheelaxis) {
        $(selector).mCustomScrollbar({
            theme: theme,
            scrollInertia: 100,
            axis: 'yx',
            mouseWheel: {
                enable: true,
                axis: mousewheelaxis,
                preventDefault: true
            }
        });
    }

    if (!$('html').hasClass('ismobile')) {
        //On Custom Class
        if ($('.c-overflow')[0]) {
            scrollBar('.c-overflow', 'minimal-dark', 'y');
        }
    }

    /*
     * Top Search
     */
    (function () {
        $('body').on('click', '#top-search > a', function (e) {
            e.preventDefault();

            $('#header').addClass('search-toggled');
            $('#top-search-wrap input').focus();
        });

        $('body').on('click', '#top-search-close', function (e) {
            e.preventDefault();

            $('#header').removeClass('search-toggled');
        });

        $('body').on('click', '#add-arsip > a, .add-arsip', function (e) {
            e.preventDefault();
            window.location.replace($(this).attr('data-default'));
        });
    })();

    /*
     * Sidebar
     */
    (function () {
        //Toggle
        $('body').on('click', '#menu-trigger, #chat-trigger', function (e) {
            e.preventDefault();
            var x = $(this).data('trigger');

            $(x).toggleClass('toggled');
            $(this).toggleClass('open');

            //Close opened sub-menus
            $('.sub-menu.toggled').not('.active').each(function () {
                $(this).removeClass('toggled');
                $(this).find('ul').hide();
            });



            $('.profile-menu .main-menu').hide();

            if (x == '#sidebar') {

                $elem = '#sidebar';
                $elem2 = '#menu-trigger';

                $('#chat-trigger').removeClass('open');

                if (!$('#chat').hasClass('toggled')) {
                    $('#header').toggleClass('sidebar-toggled');
                } else {
                    $('#chat').removeClass('toggled');
                }
            }

            if (x == '#chat') {
                $elem = '#chat';
                $elem2 = '#chat-trigger';

                $('#menu-trigger').removeClass('open');

                if (!$('#sidebar').hasClass('toggled')) {
                    $('#header').toggleClass('sidebar-toggled');
                } else {
                    $('#sidebar').removeClass('toggled');
                }
            }

            //When clicking outside
            if ($('#header').hasClass('sidebar-toggled')) {
                $(document).on('click', function (e) {
                    if (($(e.target).closest($elem).length === 0) && ($(e.target).closest($elem2).length === 0)) {
                        setTimeout(function () {
                            $($elem).removeClass('toggled');
                            $('#header').removeClass('sidebar-toggled');
                            $($elem2).removeClass('open');
                        });
                    }
                });
            }
        })

        //Submenu
        $('body').on('click', '.sub-menu > a', function (e) {
            e.preventDefault();
            $(this).next().slideToggle(200);
            $(this).parent().toggleClass('toggled');
        });
    })();

    /*
     * Clear Notification
     */
    $('body').on('click', '[data-clear="notification"]', function (e) {
        e.preventDefault();

        var x = $(this).closest('.listview');
        var y = x.find('.lv-item');
        var z = y.size();

        $(this).parent().fadeOut();

        x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
        x.find('.grid-loading').fadeIn(1500);


        var w = 0;
        y.each(function () {
            var z = $(this);
            setTimeout(function () {
                z.addClass('animated fadeOutRightBig').delay(1000).queue(function () {
                    z.remove();
                });
            }, w += 150);
        })

        //Popup empty message
        setTimeout(function () {
            $('#notifications').addClass('empty');
        }, (z * 150) + 200);
    });

    /*
     * Dropdown Menu
     */
    if ($('.dropdown')[0]) {
        //Propagate
        $('body').on('click', '.dropdown.open .dropdown-menu', function (e) {
            e.stopPropagation();
        });

        $('.dropdown').on('shown.bs.dropdown', function (e) {
            if ($(this).attr('data-animation')) {
                $animArray = [];
                $animation = $(this).data('animation');
                $animArray = $animation.split(',');
                $animationIn = 'animated ' + $animArray[0];
                $animationOut = 'animated ' + $animArray[1];
                $animationDuration = ''
                if (!$animArray[2]) {
                    $animationDuration = 500; //if duration is not defined, default is set to 500ms
                } else {
                    $animationDuration = $animArray[2];
                }

                $(this).find('.dropdown-menu').removeClass($animationOut)
                $(this).find('.dropdown-menu').addClass($animationIn);
            }
        });

        $('.dropdown').on('hide.bs.dropdown', function (e) {
            if ($(this).attr('data-animation')) {
                e.preventDefault();
                $this = $(this);
                $dropdownMenu = $this.find('.dropdown-menu');

                $dropdownMenu.addClass($animationOut);
                setTimeout(function () {
                    $this.removeClass('open')

                }, $animationDuration);
            }
        });
    }

    /*
     * Calendar Widget
     */
    if ($('#calendar-widget')[0]) {
        (function () {
            $('#calendar-widget').fullCalendar({
                contentHeight: 'auto',
                theme: true,
                header: {
                    right: '',
                    center: 'prev, title, next',
                    left: ''
                },
                defaultDate: '2014-06-12',
                editable: true,
                events: [
                    {
                        title: 'All Day',
                        start: '2014-06-01',
                        className: 'bgm-cyan'
                    },
                    {
                        title: 'Long Event',
                        start: '2014-06-07',
                        end: '2014-06-10',
                        className: 'bgm-orange'
                    },
                    {
                        id: 999,
                        title: 'Repeat',
                        start: '2014-06-09',
                        className: 'bgm-lightgreen'
                    },
                    {
                        id: 999,
                        title: 'Repeat',
                        start: '2014-06-16',
                        className: 'bgm-lightblue'
                    },
                    {
                        title: 'Meet',
                        start: '2014-06-12',
                        end: '2014-06-12',
                        className: 'bgm-green'
                    },
                    {
                        title: 'Lunch',
                        start: '2014-06-12',
                        className: 'bgm-cyan'
                    },
                    {
                        title: 'Birthday',
                        start: '2014-06-13',
                        className: 'bgm-amber'
                    },
                    {
                        title: 'Google',
                        url: 'http://google.com/',
                        start: '2014-06-28',
                        className: 'bgm-amber'
                    }
                ]
            });
        })();
    }


    /*
     * Todo Add new item
     */
    if ($('#todo-lists')[0]) {
        //Add Todo Item
        $('body').on('click', '#add-tl-item .add-new-item', function () {
            $(this).parent().addClass('toggled');
        });

        //Dismiss
        $('body').on('click', '.add-tl-actions > a', function (e) {
            e.preventDefault();
            var x = $(this).closest('#add-tl-item');
            var y = $(this).data('tl-action');

            if (y == "dismiss") {
                x.find('textarea').val('');
                x.removeClass('toggled');
            }

            if (y == "save") {
                x.find('textarea').val('');
                x.removeClass('toggled');
            }
        });
    }

    /*
     * Auto Hight Textarea
     */
    if ($('.auto-size')[0]) {
        autosize($('.auto-size'));
    }

    /*
     * Profile Menu
     */
    $('body').on('click', '.profile-menu > a', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('toggled');
        $(this).next().slideToggle(200);
    });

    /*
     * Text Feild
     */

    //Add blue animated border and remove with condition when focus and blur
    if ($('.fg-line')[0]) {
        $('body').on('focus', '.fg-line .form-control', function () {
            $(this).closest('.fg-line').addClass('fg-toggled');
        })

        $('body').on('blur', '.form-control', function () {
            var p = $(this).closest('.form-group, .input-group');
            var i = p.find('.form-control').val();

            if (p.hasClass('fg-float')) {
                if (i.length == 0) {
                    $(this).closest('.fg-line').removeClass('fg-toggled');
                }
            } else {
                $(this).closest('.fg-line').removeClass('fg-toggled');
            }
        });
    }

    //Add blue border for pre-valued fg-flot text feilds
    if ($('.fg-float')[0]) {
        $('.fg-float .form-control').each(function () {
            var i = $(this).val();

            if (!i.length == 0) {
                $(this).closest('.fg-line').addClass('fg-toggled');
            }

        });
    }

    /*
     * Audio and Video
     */
    if ($('audio, video')[0]) {
        $('video,audio').mediaelementplayer();
    }

    /*
     * Tag Select
     */
    if ($('.chosen')[0]) {
        $('.chosen').chosen({
            width: '100%',
            allow_single_deselect: true
        });
    }

    /*
     * Input Slider
     */
    //Basic
    if ($('.input-slider')[0]) {
        $('.input-slider').each(function () {
            var isStart = $(this).data('is-start');

            $(this).noUiSlider({
                start: isStart,
                range: {
                    'min': 0,
                    'max': 100,
                }
            });
        });
    }

    //Range slider
    if ($('.input-slider-range')[0]) {
        $('.input-slider-range').noUiSlider({
            start: [30, 60],
            range: {
                'min': 0,
                'max': 100
            },
            connect: true
        });
    }

    //Range slider with value
    if ($('.input-slider-values')[0]) {
        $('.input-slider-values').noUiSlider({
            start: [45, 80],
            connect: true,
            direction: 'rtl',
            behaviour: 'tap-drag',
            range: {
                'min': 0,
                'max': 100
            }
        });

        $('.input-slider-values').Link('lower').to($('#value-lower'));
        $('.input-slider-values').Link('upper').to($('#value-upper'), 'html');
    }

    /*
     * Input Mask
     */
    if ($('input-mask')[0]) {
        $('.input-mask').mask();
    }

    /*
     * Color Picker
     */
    if ($('.color-picker')[0]) {
        $('.color-picker').each(function () {
            var colorOutput = $(this).closest('.cp-container').find('.cp-value');
            $(this).farbtastic(colorOutput);
        });
    }

    /*
     * HTML Editor
     */
    if ($('.html-editor')[0]) {
        $('.html-editor').summernote({
            height: 150
        });
    }

    if ($('.html-editor-click')[0]) {
        //Edit
        $('body').on('click', '.hec-button', function () {
            $('.html-editor-click').summernote({
                focus: true
            });
            $('.hec-save').show();
        })

        //Save
        $('body').on('click', '.hec-save', function () {
            $('.html-editor-click').code();
            $('.html-editor-click').destroy();
            $('.hec-save').hide();
            notify('Content Saved Successfully!', 'success');
        });
    }

    //Air Mode
    if ($('.html-editor-airmod')[0]) {
        $('.html-editor-airmod').summernote({
            airMode: true
        });
    }

    /*
     * Date Time Picker
     */

    //Date Time Picker
    if ($('.date-time-picker')[0]) {
        $('.date-time-picker').datetimepicker();
    }

    //Time
    if ($('.time-picker')[0]) {
        $('.time-picker').datetimepicker({
            format: 'LT'
        });
    }

    //Date
    if ($('.date-picker')[0]) {
        $('.date-picker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
    }

    /*
     * Form Wizard
     */

    if ($('.form-wizard-basic')[0]) {
        $('.form-wizard-basic').bootstrapWizard({
            tabClass: 'fw-nav',
            'nextSelector': '.next',
            'previousSelector': '.previous'
        });
    }

    /*
     * Bootstrap Growl - Notifications popups
     */
    function notify(message, type) {
        $.growl({
            message: message
        }, {
            type: type,
            allow_dismiss: false,
            label: 'Cancel',
            className: 'btn-xs btn-inverse',
            placement: {
                from: 'top',
                align: 'right'
            },
            delay: 2500,
            animate: {
                enter: 'animated bounceIn',
                exit: 'animated bounceOut'
            },
            offset: {
                x: 20,
                y: 85
            }
        });
    }
    ;

    /*
     * Waves Animation
     */
//    (function () {
//        Waves.attach('.btn:not(.btn-icon):not(.btn-float)');
//        Waves.attach('.btn-icon, .btn-float', ['waves-circle', 'waves-float']);
//        Waves.init();
//    })();

    /*
     * Lightbox
     */
    if ($('.lightbox')[0]) {
        $('.lightbox').lightGallery({
            enableTouch: true
        });
    }

    /*
     * Link prevent
     */
    $('body').on('click', '.a-prevent', function (e) {
        e.preventDefault();
    });

    /*
     * Collaspe Fix
     */
    if ($('.collapse')[0]) {

        //Add active class for opened items
        $('.collapse').on('show.bs.collapse', function (e) {
            $(this).closest('.panel').find('.panel-heading').addClass('active');
        });

        $('.collapse').on('hide.bs.collapse', function (e) {
            $(this).closest('.panel').find('.panel-heading').removeClass('active');
        });

        //Add active class for pre opened items
        $('.collapse.in').each(function () {
            $(this).closest('.panel').find('.panel-heading').addClass('active');
        });
    }

    /*
     * Tooltips
     */
    if ($('[data-toggle="tooltip"]')[0]) {
        $('[data-toggle="tooltip"]').tooltip();
    }

    /*
     * Popover
     */
    if ($('[data-toggle="popover"]')[0]) {
        $('[data-toggle="popover"]').popover();
    }

    /*
     * Message
     */

    //Actions
    if ($('.on-select')[0]) {
        var checkboxes = '.lv-avatar-content input:checkbox';
        var actions = $('.on-select').closest('.lv-actions');

        $('body').on('click', checkboxes, function () {
            if ($(checkboxes + ':checked')[0]) {
                actions.addClass('toggled');
            } else {
                actions.removeClass('toggled');
            }
        });
    }

    if ($('#ms-menu-trigger')[0]) {
        $('body').on('click', '#ms-menu-trigger', function (e) {
            e.preventDefault();
            $(this).toggleClass('open');
            $('.ms-menu').toggleClass('toggled');
        });
    }

    /*
     * Login
     */
    if ($('.login-content')[0]) {
        //Add class to HTML. This is used to center align the logn box
        $('html').addClass('login-content');

        $('body').on('click', '.login-navigation > li', function () {
            var z = $(this).data('block');
            var t = $(this).closest('.lc-block');

            t.removeClass('toggled');

            setTimeout(function () {
                $(z).addClass('toggled');
            });

        })
    }

    /*
     * Fullscreen Browsing
     */
    if ($('[data-action="fullscreen"]')[0]) {
        var fs = $("[data-action='fullscreen']");
        fs.on('click', function (e) {
            e.preventDefault();

            //Launch
            function launchIntoFullscreen(element) {

                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            //Exit
            function exitFullscreen() {

                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            launchIntoFullscreen(document.documentElement);
            fs.closest('.dropdown').removeClass('open');
        });
    }

    /*
     * Clear Local Storage
     */
    if ($('[data-action="clear-localstorage"]')[0]) {
        var cls = $('[data-action="clear-localstorage"]');

        cls.on('click', function (e) {
            e.preventDefault();

            swal({
                title: "Apakah anda yakin?",
                text: "Semua yang tersimpan di penyimpanan lokal akan dihapus",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Batalkan",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, hapus semua!",
                closeOnConfirm: false
            }, function () {
                localStorage.clear();
                swal("Selesa!", "penyimpanan lokal telah dibersihkan", "sukses");
            });
        });
    }

    /*
     * Profile Edit Toggle
     */
    if ($('[data-pmb-action]')[0]) {
        $('body').on('click', '[data-pmb-action]', function (e) {
            e.preventDefault();
            var d = $(this).data('pmb-action');

            if (d === "edit") {
                $(this).closest('.pmb-block').toggleClass('toggled');
            }

            if (d === "reset") {
                $(this).closest('.pmb-block').removeClass('toggled');
            }


        });
    }

    /*
     * IE 9 Placeholder
     */
    if ($('html').hasClass('ie9')) {
        $('input, textarea').placeholder({
            customClass: 'ie9-placeholder'
        });
    }


    /*
     * Listview Search
     */
    if ($('.lvh-search-trigger')[0]) {


        $('body').on('click', '.lvh-search-trigger', function (e) {
            e.preventDefault();
            x = $(this).closest('.lv-header-alt').find('.lvh-search');

            x.fadeIn(300);
            x.find('.lvhs-input').focus();
        });

        //Close Search
        $('body').on('click', '.lvh-search-close', function () {
            x.fadeOut(300);
            setTimeout(function () {
                x.find('.lvhs-input').val('');
            }, 350);
        })
    }


    /*
     * Print
     */
    if ($('[data-action="print"]')[0]) {
        $('body').on('click', '[data-action="print"]', function (e) {
            e.preventDefault();

            window.print();
        })
    }

    /*
     * Typeahead Auto Complete
     */
    if ($('.typeahead')[0]) {

        var statesArray = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ];
        var states = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: statesArray
        });

        $('.typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
                {
                    name: 'states',
                    source: states
                });
    }


    /*
     * Wall
     */
    if ($('.wcc-toggle')[0]) {
        var z = '<div class="wcc-inner">' +
                '<textarea class="wcci-text auto-size" placeholder="Write Something..."></textarea>' +
                '</div>' +
                '<div class="m-t-15">' +
                '<button class="btn btn-sm btn-primary">Post</button>' +
                '<button class="btn btn-sm btn-link wcc-cencel">Cancel</button>' +
                '</div>'


        $('body').on('click', '.wcc-toggle', function () {
            $(this).parent().html(z);
            autosize($('.auto-size')); //Reload Auto size textarea
        });

        //Cancel
        $('body').on('click', '.wcc-cencel', function (e) {
            e.preventDefault();

            $(this).closest('.wc-comment').find('.wcc-inner').addClass('wcc-toggle').html('Write Something...')
        });

    }

    /*
     * Skin Change
     */
    $('body').on('click', '[data-skin]', function () {
        var currentSkin = $('[data-current-skin]').data('current-skin');
        var skin = $(this).data('skin');

        $('[data-current-skin]').attr('data-current-skin', skin)

    });

    $('form#cfrm').submit(function () {
        var w = window.location;
        var p = w.pathname;
        var a = p.split('/')
        var uri = 'http://' + w.hostname + '/' + a[1] + '/'; // + a[2] + '/';
        console.log(uri)

        var link = uri + 'cari/';
        var sData = $('#cfrm').serialize();
        $.ajax({
            url: link,
            type: "POST",
            data: sData,
            dataType: "html",
            beforeSend: function () {
                $(".content .container").isLoading({
                    text: "Proses Cari",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
            },
            success: function (isi) {
                setTimeout(function () {
                    $(".container").html('').html(isi);
                    $('#header').removeClass('search-toggled');
                    $(".content .container").isLoading("hide");
                }, 1000);
            },
            error: function () {
                setTimeout(function () {
                    $('#header').removeClass('search-toggled');
                    $(".content .container").isLoading("hide");
                }, 1000);
            }
        });
        return false;
    });

});
function loadTabelGlobal(k, u, p) {
//    if (u.substr(0, 1) == '.') {
//        var img = '../addons/img/loading_small.gif';
//    } else {
//        var img = 'addons/img/loading_small.gif';
//    }
    var opt = {
        aLengthMenu: [[10, 20, 30, 50, -1], [10, 20, 30, 50, "All"]],
        sDom: "<'row'<'col-xs-5'l><'col-xs-7'fB>r<'clear'>>t<'row'<'col-xs-5'i><'col-xs-7'p>>",
        buttons: [
            'copy', 'excel'
        ],
        bProcessing: true,
        bServerSide: true,
        retrieve: true,
        responsive: true,
        oLanguage: {
            sLoadingRecords: "Tunggu sejenak - memuat..."
            , sProcessing: '<div class="preloader pls-amber" ><svg class="pl-circular" style="top:20px;" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>Sedang Proses'
            , sSearch: "<span>Pencarian:</span> "
            , sLengthMenu: "_MENU_ data per halaman"
            , sInfo: "Menampilkan <span>_START_</span> s/d <span>_END_</span> dari <span>_TOTAL_</span> data"
            , sEmptyTable: "Tidak ada data"
            , sInfoEmpty: "Tidak ada data"
            , sZeroRecords: "Tidak ada data"
            , oPaginate: {
                sFirst: "<<"
                , sLast: ">>"
                , sNext: ">"
                , sPrevious: "<"}},
        sAjaxSource: u + "/getData/" + $('#tabel').val() + "/" + p
        , fnServerData: function (sSource, aoData, fnCallback, oSettings) {
            scrollTo();
            oSettings.jqXHR = $.ajax({dataType: "json", type: "POST", url: sSource, data: aoData, success: fnCallback})
        },
//            "order": [],
        aoColumnDefs: [
            {aTargets: [(k - 1)], mRender: function (data, type, row) {
                    var identifier = ($('#tabel').val() == "matakuliah") ? row[0] + data : row[0];
                    var fn = (/[.]/.exec(data)) ? /[^.]+$/.exec(data) : undefined;
                    var btn = "";
                    if (fn !== undefined) {
                        btn = btn + '<button class="btn btn-info" id="view" data-toggle="tooltip" title="View" data-value="' + identifier + '" data-get="" data-default="' + data + '"><i class="zmdi zmdi-collection-pdf"></i></button> ';
                    }
                    var btn = btn + '<button class="btn btn-warning" id="edit" data-toggle="tooltip" title="Edit" data-value="' + identifier + '" data-get="' + u + '/edit" data-default="' + $('#tabel').val() + '"><i class="zmdi zmdi-edit"></i></button> ';
                    var btn = btn + '<button class="btn btn-danger" id="hapus" data-toggle="tooltip" title="Hapus" data-value="' + identifier + '"  data-get="' + u + '/hapus" data-default="' + row[1] + '" data-mtabel="' + $('#tabel').val() + '"><i class="zmdi zmdi-delete"></i></button> ';
                    var btn = btn + '<button class="btn btn-default" id="salin" data-toggle="tooltip" title="Salin" data-value="' + identifier + '"  data-get="' + u + '/salin" data-default="salin" data-mtabel="' + $('#tabel').val() + '"><i class="zmdi zmdi-copy"></i></button> ';
                    var retStr = btn;
                    return retStr
                }
                , sClass: "center p-r-0"
                , sWidth: "125px"}
        ],
        initComplete: function (settings, json) {

        },
        fnDrawCallback: function (settings) {
            scrollTo();
        }
    };
    if (myApp.oTable != null)
        myApp.oTable.fnDestroy();
    myApp.oTable = $("#data-table-basic").dataTable(opt);
    $(".dataTables_filter input").attr("placeholder", "cari..");
    $(".dataTables_paginate, .dataTables_filter").addClass("pull-right");
    $(".dataTables_processing").css({position: "relative", width: "23%", left: "36%", "text-align": "center", "color": "#fff", "background": "rgba(0,0,0,.6)", "display": "inline-block", "font-size": "2em", "border-radius": "7px", "box-shadow": "0px 0px 7px #000", "padding-bottom": "10px"});
//    $(".dataTables_processing").css({position: "absolute", width: "200px", height: "auto", right: "15px", top: "25px", "text-align": "right", "color": "#fff"});
    $("#data-table-basic.table").on("click", ".btn", function (e) {
        var cmd = $(this).attr("id");
        switch (cmd) {
            case 'edit':
//                $('#divsatu').slideUp();
                $(".row #divsatu").isLoading({
                    text: "Memuat..",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
                var p = $(this).attr("data-value");
                var q = p.replace('/', '_');
                $.when($("#divform").load(u + "/loadForm/" + $(this).attr("data-default") + "/" + q)).done(function (a) {
                    $('#divsatu').slideUp();
//                    $('#divdua').slideDown();
                    $(".row #divsatu").isLoading('hide');
                    $('#divdua').slideDown();
                });
                break;
            case 'salin':
                $(".row #divsatu").isLoading({
                    text: "Memuat..",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
                var p = $(this).attr("data-value");
                var q = p.replace('/', '_');
                $.when($("#divform").load(u + "/loadForm/" + $(this).attr("data-mtabel") + "/" + q + "/" + $(this).attr("data-default"))).done(function (a) {
                    $('#divsatu').slideUp();
                    $(".row #divsatu").isLoading('hide');
                    $('#divdua').slideDown();
                });
                break;
            case 'hapus':
                $('#getto').val($(this).attr("data-get"));
                $('#aepYes').text('Hapus');
                $("#cid").val($(this).attr("data-value"));
                $("#cod").val($(this).attr("data-mtabel"));
                $('.lblModal').text('hapus ' + $(this).attr("data-default"));
                $('#myConfirm').modal();
                break;
            case 'view':
                var f = 'publik/Lampiran_' + $(this).attr("data-default");
                $.ajax({
                    url: f,
                    type: 'HEAD',
                    error: function ()
                    {
                        alert('file tidak ditemukan.');
                    },
                    success: function ()
                    {
                        $("#showDetilPDF").hide();
                        $("#getpdf").attr("data", f)
                        $("#getpdf").attr("src", f).show();

                        $("#showFile").modal("show");
                    }
                });


                break;
        }
        return false;
    });
}
function loadTabelGlobalCIS(k, u, p) {
    var opt = {
        aLengthMenu: [[10, 20, 30, 50, -1], [10, 20, 30, 50, "All"]],
        sDom: "<'row'<'col-xs-5'l><'col-xs-7'fB>r<'clear'>>t<'row'<'col-xs-5'i><'col-xs-7'p>>",
        buttons: [
            'copy', 'excel'
        ],
        bProcessing: true,
        bServerSide: true,
        retrieve: true,
        responsive: true,
        oLanguage: {
            sLoadingRecords: "Tunggu sejenak - memuat..."
            , sSearch: "<span>Pencarian:</span> "
            , sLengthMenu: "_MENU_ data per halaman"
            , sInfo: "Menampilkan <span>_START_</span> s/d <span>_END_</span> dari <span>_TOTAL_</span> data"
            , sEmptyTable: "Tidak ada data"
            , sInfoEmpty: "Tidak ada data"
            , sZeroRecords: "Tidak ada data"
            , oPaginate: {
                sFirst: "<<"
                , sLast: ">>"
                , sNext: ">"
                , sPrevious: "<"}},
        sAjaxSource: u + "/getData/" + $('#tabel').val() + "/" + p
        , fnServerData: function (sSource, aoData, fnCallback, oSettings) {
            scrollTo();
            oSettings.jqXHR = $.ajax({dataType: "json", type: "POST", url: sSource, data: aoData, success: fnCallback})
        },
//            "order": [],
        aoColumnDefs: [
            {aTargets: [(k - 1)], mRender: function (data, type, row) {
                    var identifier = ($('#tabel').val() == "matakuliah") ? row[0] + data : row[0];
                    var fn = (/[.]/.exec(data)) ? /[^.]+$/.exec(data) : undefined;
                    var btn = "";
                    if (fn !== undefined) {
                        btn = btn + '<button class="btn btn-info" id="view" data-toggle="tooltip" title="View" data-value="' + identifier + '" data-get="" data-default="' + data + '"><i class="zmdi zmdi-collection-pdf"></i></button> ';
                    }
                    var btn = btn + '<button class="btn btn-warning" id="edit" data-toggle="tooltip" title="Edit" data-value="' + identifier + '" data-get="' + u + '/edit" data-default="' + $('#tabel').val() + '"><i class="fa fa-edit"></i></button> ';
                    var btn = btn + '<button class="btn btn-danger" id="hapus" data-toggle="tooltip" title="Hapus" data-value="' + identifier + '"  data-get="' + u + '/hapus" data-default="' + row[1] + '" data-mtabel="' + $('#tabel').val() + '"><i class="fa fa-trash"></i></button> ';
                    var btn = btn + '<button class="btn btn-secondary" id="salin" data-toggle="tooltip" title="Salin" data-value="' + identifier + '"  data-get="' + u + '/salin" data-default="salin" data-mtabel="' + $('#tabel').val() + '"><i class="fa fa-copy"></i></button> ';
                    var retStr = btn;
                    return retStr
                }
                , sClass: "center p-r-0"
                , sWidth: "150px"}
        ],
        initComplete: function (settings, json) {

        },
        fnDrawCallback: function (settings) {
//            scrollTo();
        }
    };
    if (myApp.oTable != null)
        myApp.oTable.fnDestroy();
    myApp.oTable = $("#data-table-basic").dataTable(opt);
    $(".dataTables_filter input").attr("placeholder", "cari..");
    $(".dataTables_paginate, .dataTables_filter").addClass("pull-right");
    $(".dataTables_processing").css({width: "200px", height: "auto", right: "85px", top: "50px", "text-align": "center", "margin-left": "533px"});
    $("#data-table-basic.table").on("click", ".btn", function (e) {
        var cmd = $(this).attr("id");
        switch (cmd) {
            case 'edit':
//                $('#divsatu').slideUp();
                $(".row #divsatu").isLoading({
                    text: "Memuat..",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
                var p = $(this).attr("data-value");
                var q = p.replace('/', '_');
                $.when($("#divform").load(u + "/loadForm/" + $(this).attr("data-default") + "/" + q)).done(function (a) {
                    $('#divsatu').slideUp();
//                    $('#divdua').slideDown();
                    $(".row #divsatu").isLoading('hide');
                    $('#divdua').slideDown('fast');
                });
                break;
            case 'salin':
                $(".row #divsatu").isLoading({
                    text: "Memuat..",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
                var p = $(this).attr("data-value");
                var q = p.replace('/', '_');
                $.when($("#divform").load(u + "/loadForm/" + $(this).attr("data-mtabel") + "/" + q + "/" + $(this).attr("data-default"))).done(function (a) {
                    $('#divsatu').slideUp('fast');
                    $(".row #divsatu").isLoading('hide');
                    $('#divdua').slideDown('fast');
                });
                break;
            case 'hapus':
                $('#getto').val($(this).attr("data-get"));
                $('#aepYes').text('Hapus');
                $("#cid").val($(this).attr("data-value"));
                $("#cod").val($(this).attr("data-mtabel"));
                $('.lblModal').text('hapus ' + $(this).attr("data-default"));
                $('#myConfirm').modal();
                break;
            case 'view':
                var f = 'publik/Lampiran_' + $(this).attr("data-default");
                $.ajax({
                    url: f,
                    type: 'HEAD',
                    error: function ()
                    {
                        alert('file tidak ditemukan.');
                    },
                    success: function ()
                    {
                        $("#showDetilPDF").hide();
                        $("#getpdf").attr("data", f)
                        $("#getpdf").attr("src", f).show();
                        $("#showFile").modal("show");
                    }
                });


                break;
        }
        return false;
    });
}
function loadTabelSeq(k, u, p, myTabel, t) {
    var opt = {
        aLengthMenu: [[10, 20, 30, 50, -1], [10, 20, 30, 50, "All"]],
        sDom: "<'row'<'col-xs-5'l><'col-xs-7'fB>r<'clear'>>t<'row'<'col-xs-5'i><'col-xs-7'p>>",
        buttons: [
            'copy', 'excel'
        ],
        bProcessing: true,
        bServerSide: true,
        retrieve: true,
        responsive: false,
        oLanguage: {
            sLoadingRecords: "Tunggu sejenak - memuat..."
            , sProcessing: '<div class="preloader pls-amber" ><svg class="pl-circular" style="top:20px;" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>Sedang Proses'
            , sSearch: "<span>Pencarian:</span> "
            , sLengthMenu: "_MENU_ data per halaman"
            , sInfo: "Menampilkan <span>_START_</span> s/d <span>_END_</span> dari <span>_TOTAL_</span> data"
            , sEmptyTable: "Tidak ada data"
            , sInfoEmpty: "Tidak ada data"
            , sZeroRecords: "Tidak ada data"
            , oPaginate: {
                sFirst: "<<"
                , sLast: ">>"
                , sNext: ">"
                , sPrevious: "<"}},
        sAjaxSource: u + "/getData/" + $('#tabel').val() + "/" + p
        , fnServerData: function (sSource, aoData, fnCallback, oSettings) {
            scrollTo();
            oSettings.jqXHR = $.ajax({dataType: "json", type: "POST", url: sSource, data: aoData, success: fnCallback})
        },
//            "order": [],
        aoColumnDefs: [
            {aTargets: [(k - 1)], mRender: function (data, type, row) {
                    var identifier = ($('#tabel').val() == "matakuliah") ? row[0] : row[0]; //row[0] + '-' + data
                    var fn = (/[.]/.exec(data)) ? /[^.]+$/.exec(data) : undefined;
                    var btn = "";
                    if (fn !== undefined) {
                        btn = btn + '<button class="btn btn-info" id="view" data-toggle="tooltip" title="View" data-value="' + identifier + '" data-get="" data-default="' + data + '"><i class="zmdi zmdi-collection-pdf"></i></button> ';
                    }
                    var btn = btn + '<button class="btn btn-warning" id="edit" data-toggle="tooltip" title="Edit" data-tabs="' + p + '" data-value="' + identifier + '" data-get="' + u + '/edit" data-default="' + $('#tabel').val() + '"><i class="zmdi zmdi-edit"></i></button> ';
                    var btn = btn + '<button class="btn btn-danger" id="hapus" data-toggle="tooltip" title="Hapus" data-tabs="' + p + '" data-value="' + identifier + '"  data-get="' + u + '/hapus" data-default="' + row[1] + '" data-mtabel="' + $('#tabel').val() + '_' + data + '"><i class="zmdi zmdi-delete"></i></button> ';
                    var btn = btn + '<button class="btn btn-default" id="salin" data-toggle="tooltip" title="Salin" data-tabs="' + p + '" data-value="' + identifier + '"  data-get="' + u + '/salin" data-default="salin" data-mtabel="' + $('#tabel').val() + '"><i class="zmdi zmdi-copy"></i></button> ';
                    var retStr = btn;
                    return retStr
                }
                , sClass: "center p-r-0"
                , sWidth: "125px"}
        ],
        initComplete: function (settings, json) {

        },
        fnDrawCallback: function (settings) {
            scrollTo();
        }
    };
    if (myTabel.oTable != null)
        myTabel.oTable.fnDestroy();
    myTabel.oTable = t.dataTable(opt);
    $(".dataTables_filter input").attr("placeholder", "cari..");
    $(".dataTables_paginate, .dataTables_filter").addClass("pull-right");
    $(".dataTables_processing").css({position: "relative", width: "23%", left: "36%", "text-align": "center", "color": "#fff", "background": "rgba(0,0,0,.6)", "display": "inline-block", "font-size": "2em", "border-radius": "7px", "box-shadow": "0px 0px 7px #000", "padding-bottom": "10px"});
//    $(".dataTables_processing").css({position: "absolute", width: "200px", height: "auto", right: "15px", top: "25px", "text-align": "right", "color": "#fff"});
    t.on("click", ".btn", function (e) {
        var cmd = $(this).attr("id");
        switch (cmd) {
            case 'edit':
//                $('#divsatu').slideUp();
                $(".row #divsatu").isLoading({
                    text: "Memuat..",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
                var p = $(this).attr("data-value");
                var q = p.replace('/', '_');
                $.when($("#divform").load(u + "/loadForm/" + $(this).attr("data-default") + "/" + q + "/" + $(this).attr("data-tabs"))).done(function (a) {
                    $('#divsatu').slideUp();
                    $(".row #divsatu").isLoading('hide');
                    $('#divdua').slideDown();
                });
                break;
            case 'salin':
                $(".row #divsatu").isLoading({
                    text: "Memuat..",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
                var p = $(this).attr("data-value");
                var q = p.replace('/', '_');
                $.when($("#divform").load(u + "/loadForm/" + $(this).attr("data-mtabel") + "/" + q + "/" + $(this).attr("data-default") + "/" + $(this).attr("data-tabs"))).done(function (a) {
                    $('#divsatu').slideUp();
                    $(".row #divsatu").isLoading('hide');
                    $('#divdua').slideDown();
                });
                break;
            case 'hapus':
                $('#getto').val($(this).attr("data-get"));
                $('#aepYes').text('Hapus');
                $("#cid").val($(this).attr("data-value"));
                $("#cod").val($(this).attr("data-mtabel"));
                $('.lblModal').text('hapus ' + $(this).attr("data-default"));
                $('#myConfirm').modal();
                break;
            case 'view':
                var f = 'publik/Lampiran_' + $(this).attr("data-default");
                $.ajax({
                    url: f,
                    type: 'HEAD',
                    error: function ()
                    {
                        alert('file tidak ditemukan.');
                    },
                    success: function ()
                    {
                        $("#showDetilPDF").hide();
                        $("#getpdf").attr("data", f)
                        $("#getpdf").attr("src", f).show();

                        $("#showFile").modal("show");
                    }
                });


                break;
        }
        return false;
    });
}
function myTable(table, costumize) {
    var option = {
        displayLength: 10,
        scrollX: true,
        paging: true,
        dom: "<'row'<'col-xs-5'l><'col-xs-7'f>r<'clear'>>t<'row'<'col-xs-6'i><'col-xs-6'p>>",
        language: {
            loadingRecords: "Tunggu sejenak - memuat...",
            search: "<span>Pencarian:</span> ",
            lengthMenu: "_MENU_ data per halaman",
            info: "Menampilkan <span>_START_</span> s/d <span>_END_</span> dari <span>_TOTAL_</span> data",
            emptyTable: "Tidak ada data",
            infoEmpty: "Tidak ada data",
            zeroRecords: "Tidak ada data",
            paginate: {
                next: '<i class="zmdi zmdi-chevron-right"></i>',
                previous: '<i class="zmdi zmdi-chevron-left"></i>'}
        },
        autoWidth: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        responsive: false,
        fnServerData: function (sSource, aoData, fnCallback, oSettings) {
            oSettings.jqXHR = $.ajax({dataType: "json", type: "POST", url: sSource, data: aoData, success: fnCallback})
        },
        fnDrawCallback: function (settings) {
            scrollTo();
            $('select').selectpicker();
        }
    };

    var all = $.extend(option, costumize);
//    $(table).dataTable(all);

    if (myApp.oTable != null)
        myApp.oTable.fnDestroy();
    myApp.oTable = $(table).dataTable(all);

    $(".dataTables_filter").addClass('pull-right m-r-10');
    $(".dataTables_filter input").attr("placeholder", "kata pencarian..");
    $(".dataTables_filter input").addClass("search-field");
    $(".dataTables_info").addClass('p-l-20 p-t-25 p-b-25');
    $(".dataTables_length").addClass('p-l-20');
    $(".dataTables_paginate").addClass("m-r-20");
    $(".dataTables_processing").css({position: "relative", width: "200px", height: "auto", right: "85px", top: "20px", "text-align": "right"});
    $(".table").on("click", ".btn", function (e) {
        var cmd = $(this).attr("data-bind");
        var sumber = $(this).attr("data-tabel");
        var t = '#table' + $("#tabel").val();
        var f = '#form' + $("#tabel").val();
        switch (cmd) {
            case 'ubah':
//                console.log(f + ' ' + sumber + '/form/' + $("#tabel").val() + "/" + $(this).attr("data-kode"))                
                $(t).hide();
//                scrollTo();
                $.when($(f).load('' + sumber + '/form/' + $("#tabel").val() + "/" + $(this).attr("data-kode"))).done(function () {
                    setTimeout(function () {
                        $('#containerform').show();
                        scrollTo();
                    }, 300);
                });
//                $(f).load('' + sumber + '/form/' + $("#tabel").val() + "/" + $(this).attr("data-kode"));

                break;
            case 'salin':
                $(".row #containerform").isLoading({
                    text: "Memuat..",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
                $.when($(f).load('' + $(this).attr("data-get") + '/' + $("#tabel").val() + "/" + $(this).attr("data-kode") + "/salin")).done(function (a) {
                    $(t).hide();
                    setTimeout(function () {
                        $(".row #containerform").isLoading('hide');
                        $('#containerform').show();
                        scrollTo();
                    }, 300);
                });
                break;
            case 'hapus':
                $('#getto').val($(this).attr("data-get"));
                $('#aepYes').text('Hapus');
                $("#cid").val($(this).attr("data-value"));
                $("#cod").val($(this).attr("data-mtabel"));
                $('.lblModal').text('hapus ' + $(this).attr("data-default"));
                $('#myConfirm').modal();
                break;
            case 'view':

                break;
        }
    });
}
function myTableCIS(table, costumize, monitor) {
    var option = {
        displayLength: 10,
        scrollX: true,
        paging: true,
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>r<'clear'>>t<'row'<'col-sm-5'i><'col-sm-7'p>>",
        language: {
            loadingRecords: "Tunggu sejenak - memuat...",
            search: "<span>Pencarian:</span> ",
            lengthMenu: "_MENU_ data per halaman",
            info: "Menampilkan <span>_START_</span> s/d <span>_END_</span> dari <span>_TOTAL_</span> data",
            emptyTable: "Tidak ada data",
            infoEmpty: "Tidak ada data",
            zeroRecords: "Tidak ada data",
            paginate: {
                next: '<i class="fa fa-chevron-right"></i>',
                previous: '<i class="fa fa-chevron-left"></i>'}
        },
        autoWidth: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        responsive: false,
        fnServerData: function (sSource, aoData, fnCallback, oSettings) {
            oSettings.jqXHR = $.ajax({dataType: "json", type: "POST", url: sSource, data: aoData, success: fnCallback})
        },
        fnDrawCallback: function (settings) {
            scrollTo();
        }
    };

    var all = $.extend(option, costumize);
    $(table).DataTable(all);
    $(".dataTables_processing").css({width: "200px", height: "auto", right: "85px", top: "50px", "text-align": "center"});
    $(".table").on("click", ".btn", function (e) {
        var cmd = $(this).attr("data-bind");
        var sumber = $(this).attr("data-tabel");
        var t = '#table' + $("#tabel").val();
        var f = '#form' + $("#tabel").val();
        switch (cmd) {
            case 'detail':
                alert("OY DETAIL");
                break;
            case 'ubah':
                $(t).hide();
                $.when($(f).load('' + sumber + '/form/' + $("#tabel").val() + "/" + $(this).attr("data-kode"))).done(function () {
                    setTimeout(function () {
                        $('#containerform').show();
                        scrollTo();
                    }, 300);
                });
                break;
            case 'salin':
                $(".row #containerform").isLoading({
                    text: "Memuat..",
                    position: "overlay",
                    tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                });
                $.when($(f).load('' + $(this).attr("data-get") + '/' + $("#tabel").val() + "/" + $(this).attr("data-kode") + "/salin")).done(function (a) {
                    $(t).hide();
                    setTimeout(function () {
                        $(".row #containerform").isLoading('hide');
                        $('#containerform').show();
                        scrollTo();
                    }, 300);
                });
                break;
            case 'hapus':
                $('#getto').val($(this).attr("data-get"));
                $('#aepYes').text('Hapus');
                $("#cid").val($(this).attr("data-value"));
                $("#cod").val($(this).attr("data-mtabel"));
                $('.lblModal').text('hapus ' + $(this).attr("data-default"));
                $('#myConfirm').modal();
                break;
            case 'view':

                break;
        }
    });
}

function scrollTo() {
    jQuery('html,body').animate({
        scrollTop: 10
    }, 'slow');
}
function gulungKe(a) {
    jQuery('' + a).animate({
        scrollTop: 40
    }, 'slow');
}
function scrollKa(target) {
    $('html,body').animate({
        scrollTop: target.offset().top
    }, 300);
}
function fgline() {
    $('body').on('focus', '.fg-line .form-control', function () {
        $(this).closest('.fg-line').addClass('fg-toggled');
    })

    $('body').on('blur', '.form-control', function () {
        var p = $(this).closest('.form-group, .input-group');
        var i = p.find('.form-control').val();

        if (p.hasClass('fg-float')) {
            if (i.length == 0) {
                $(this).closest('.fg-line').removeClass('fg-toggled');
            }
        } else {
            $(this).closest('.fg-line').removeClass('fg-toggled');
        }
    });
}
function sparklineLine(id, values, width, height, lineColor, fillColor, lineWidth, maxSpotColor, minSpotColor, spotColor, spotRadius, hSpotColor, hLineColor) {
    $('.' + id).sparkline(values, {
        type: 'line',
        width: width,
        height: height,
        lineColor: lineColor,
        fillColor: fillColor,
        lineWidth: lineWidth,
        maxSpotColor: maxSpotColor,
        minSpotColor: minSpotColor,
        spotColor: spotColor,
        spotRadius: spotRadius,
        highlightSpotColor: hSpotColor,
        highlightLineColor: hLineColor
    });
}
function updatePage(hal, html) {
    window.setTimeout(function () {
        $(hal).html(html);
    }, 300)
}
function notify(message, type) {
    $.growl({
        message: message
    }, {
        type: type,
        allow_dismiss: false,
        label: 'Cancel',
        className: 'btn-xs btn-inverse',
        placement: {
            from: 'top',
            align: 'right'
        },
        delay: 2500,
        animate: {
            enter: 'animated bounceIn',
            exit: 'animated bounceOut'
        },
        offset: {
            x: 20,
            y: 85
        }
    });
}
function removeKoma(a) {
    try {
        return a.replace(/\,/g, "")
    } catch (exception) {  // (2)
        return a;
    }


}
function addKoma(b) {
    b = removeKoma(b);
    var d = 0;
    var l = new String(b);
    l = l.split(".").reverse();
    if (l.length == 1) {
        var c = new String(b);
        c = c.split("").reverse();
        var d = "00"
    } else {
        var c = new String(l[1]);
        c = c.split("").reverse();
        var d = l[0] // roundNumber(l[0],2)
    }
    var a = "";
    for (var k = 0; k <= c.length - 1; k++) {
        a = c[k] + a;
        if (a == "-") {
            a = a
        } else {
            if (((k + 1) % 3) == 0 && c.length - 1 !== k) {
                a = "," + a
            } else {
                a = a
            }
        }

    }
    a = a + "." + d;
    return a
}
function addTitik(b) {
    var c = new String(b);
    c = c.split("").reverse();
    var d = "";
    for (var a = 0; a <= c.length - 1; a++) {
        d = c[a] + d;
        if ((a + 1) < 4) {
            d = d
        } else {
            if ((a + 1) % 3 == 0 && c.length - 1 !== a) {
                d = "." + d
            }
        }
    }
    return d
}
function piceunSpasi(a) {
    try {
        return a.replace(/\ /g, "")
    } catch (exception) {  // (2)
        return a;
    }


}

function getList(a, b, c, d, e, f, h, uri) {
//    var uri = '../../prodi/getList/';
    $.ajax({
        type: "POST",
        url: uri,
//        dataType: "json",
        data: {tabel: a, param: c, fld: d, kolom: f}
    }).done(function (data) {
        json = eval(data);
        $('#' + b).html('');
        $('#' + b).append('<option value="">' + h + '</option>');
        $(json).each(function () {
            var select = (this.id == e) ? 'selected="selected"' : '';
            $('#' + b).append('<option value="' + this.id + '" ' + select + '>' + this.id + ' ' + this.value + '</option>');
        });
        $('#' + b).selectpicker('refresh');
    });
}

function getListTahun(a, b, c, d, e, f, h, uri) {
//    var uri = '../../prodi/getList/';
    $.ajax({
        type: "POST",
        url: uri,
//        dataType: "json",
        data: {tabel: a, param: c, fld: d, kolom: f}
    }).done(function (data) {
        json = eval(data);
        $('#' + b).html('');
        $('#' + b).append('<option value="">' + h + '</option>');
        $(json).each(function () {
            var select = (this.id == e) ? 'selected="selected"' : '';
            $('#' + b).append('<option value="' + this.id + '" ' + select + '>' + this.value + '</option>');
        });
        $('#' + b).selectpicker('refresh');
    });
}
function getListX(a, b, c, d, e, f, h, uri) {
//    var uri = '../../prodi/getListX/';
    $.ajax({
        type: "POST",
        url: uri,
//        dataType: "json",
        data: {tabel: a, param: c, fld: d, kolom: f}
    }).done(function (data) {
        json = eval(data);
        $('#' + b).html('');
        $('#' + b).append('<option value="">' + h + '</option>');
        $(json).each(function () {
            var select = (this.id == e) ? 'selected="selected"' : '';
            $('#' + b).append('<option value="' + this.id + '" ' + select + ' data-bind="' + this.nomor + '">' + this.id + ' ' + this.value + '</option>');
        });
        $('#' + b).selectpicker('refresh');
    });
}