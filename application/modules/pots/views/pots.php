<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Incident Datin</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" id="import"><i class="fa fa-upload"></i></button>
                        <button type="button" class="btn btn-box-tool" id="add"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table id="data-datin" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <?php
                                if ($kolom) {
                                    foreach ($kolom as $key => $value) {
                                        if (strlen($value) == 0) {
                                            echo '<th data-type="numeric"></th>';
                                        } else {
                                            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                        }
                                    }
                                }
                                ?>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <?php
                                if ($kolom) {
                                    foreach ($kolom as $key => $value) {
                                        if (strlen($value) == 0) {
                                            echo '<th data-type="numeric"></th>';
                                        } else {
                                            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                        }
                                    }
                                }
                                ?>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($rowdata as $row): ?>
                                <tr>
                                    <td><?= $row->kode; ?></td>
                                    <td><?= $row->service_id; ?></td>
                                    <td><?= $row->top_priority; ?></td>
                                    <td><?= $row->nama_tech; ?></td>
                                    <td><?= $row->actual_solution; ?></td>
                                    <td><?= $row->compliance; ?></td>
                                    <td><?= $row->nama_problem; ?></td>
                                    <td><?= $row->nama_act; ?></td>
                                    <td>
                                        <button id="btnNgisi" class="btn btn-warning btn-md btnNgisi" data-id="<?= $row->kode; ?>"><i class="fa fa-edit"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade text-xs-left" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form role="form" id="sendFormCheck" enctype="multipart/form-data"  class="form-horizontal">
                <div class="modal-header">
                    <h4 class="modal-title">Check Tiket</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">No. Tiket</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="notik" id="notik">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">Service ID</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="service_id" id="service_id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">Witel</label>
                        <div class="col-md-6">
                            <select name="kode_witel" class="form-control select2" >
                                <option value="">- Pilihan -</option>
                                <?php
                                $q = $this->Data_model->selectData("m_witel", "kode");
                                foreach ($q as $row):
                                    ?>
                                    <option value="<?= $row->kode; ?>"><?= $row->nama_witel; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="check">Check</button>
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade text-xs-left" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form  role="form" id="sendForm" enctype="multipart/form-data"  class="form-horizontal">
                <div class="modal-header">
                    <h4 class="modal-title">Import Data</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">Week</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="week" id="file">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">File</label>
                        <div class="col-md-4">
                            <input type="file" name="file" id="file">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade text-xs-left" id="modalNgisi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form  role="form" id="sendFormManual" enctype="multipart/form-data"  class="form-horizontal">
                <div class="modal-header">
                    <h4 class="modal-title">Input Data</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="kode" name="kode">
                    <div class="form-group">
                        <label class="col-md-4 label-control" for="exampleInputFile">Technology</label>
                        <div class="col-md-6">
                            <select name="kode_tech" class="form-control select2">
                                <option value="">- Pilihan -</option>
                                <?php
                                $q = $this->Data_model->selectData("m_technology", "kode");
                                foreach ($q as $row):
                                    ?>
                                    <option value="<?= $row->kode; ?>"><?= $row->nama_tech; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 label-control" for="exampleInputFile">Top Priority</label>
                        <div class="col-md-6">
                            <select name="top_priority" class="form-control select2">
                                <option value="">- Pilihan -</option>
                                <option value="TOP100 DBS"> TOP100 DBS </option>
                                <option value="TOP100 DGS"> TOP100 DGS </option>
                                <option value="TOP100 VIP"> TOP100 VIP </option>
                                <option value="TOP20"> TOP20 </option>
                                <option value="TOP20 DES"> TOP20 DES</option>
                                <option value="TOP20 DGS"> TOP20 DGS</option>
                                <option value="TOP200"> TOP200 </option>
                                <option value="TOP200 DES"> TOP200 DES</option>
                                <option value="TOP20"> TOP20VVIP </option>
                                <option value="TOP500"> TOP500 </option>
                                <option value="VIP"> VIP </option>
                                <option value="OTHERS DBS"> OTHERS DBS </option>
                                <option value="OTHERS DES"> OTHERS DES </option>
                                <option value="OTHERS DGS"> OTHERS DGS </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 label-control" for="exampleInputFile">Compliance</label>
                        <div class="col-md-6">
                            <select name="compliance" class="form-control select2">
                                <option value="">- Pilihan -</option>
                                <option value="COMPLY"> COMPLY </option>
                                <option value="NOT COMPLY"> NOT COMPLY </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 label-control" for="exampleInputFile">Problem</label>
                        <div class="col-md-6">
                            <select name="kode_problem" class="form-control select2">
                                <option value="">- Pilihan -</option>
                                <?php
                                $q = $this->Data_model->selectData("m_problem", "kode");
                                foreach ($q as $row):
                                    ?>
                                    <option value="<?= $row->kode; ?>"><?= $row->nama_problem; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 label-control" for="exampleInputFile">Group Act Solution</label>
                        <div class="col-md-6">
                            <select name="kode_groupact" class="form-control select2">
                                <option value="">- Pilihan -</option>
                                <?php
                                $q = $this->Data_model->selectData("m_groupact", "kode");
                                foreach ($q as $row):
                                    ?>
                                    <option value="<?= $row->kode; ?>"><?= $row->nama_act; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 label-control" for="exampleInputFile">Exclude</label>
                        <div class="col-md-6">
                            <input type="checkbox" class="flat-red" checked name="exclude" value="EXCLUDE"> EXCLUDE
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>