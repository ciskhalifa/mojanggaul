<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<script src="<?= base_url('assets/jquery.isloading.min.js'); ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/bower_components/select2/dist/css/select2.min.css'); ?>">
<!-- Select2 -->
<script src="<?= base_url('assets/bower_components/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?= base_url('assets/plugins/iCheck/all.css'); ?>">
<!-- iCheck 1.0.1 -->
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js'); ?>"></script>
<script>
    var myApp = myApp || {};
    $(function () {
        $('#data-datin-cari').DataTable();
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })
        $('select').select2({width: '100%'});
        var table = $('#data-datin').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false
        });
        $('#data-datin tfoot th').each(function () {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" />');
        });
        table.columns().every(function () {
            var that = this;
            $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that.search(this.value).draw();
                }
            });
        });
        $('#import').on('click', function () {
            $('#modalImport').modal();
        });
        $('#add').on('click', function () {
            $('#modalForm').modal();
        });
        $('.btnNgisi').on('click', function () {
            $('#modalNgisi').modal();
            $('#kode').val($(this).data('id'));
        });

        $("#file").on("change", function () {
            var file = this.files[0];
            var fileName = file.name;
            var fileType = file.type;
            console.log(fileType);
            if (fileType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || fileType == "application/vnd.ms-excel") {
                $('#label').text(fileName);
                $('#reset').removeClass('hidden');
            } else {
                alert("Maaf format dokumen tidak sesuai");
                $(this).val('');
                $('#label').text('Pilih File');
            }
        });
        $('#sendFormCheck').on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'telegaul/cekDataClose/';
                var data = $("#sendFormCheck").serialize();
                ;
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    beforeSend: function (data) {
                        $("body").isLoading({
                            text: "",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                        });

                    },
                    success: function (html) {
                        check(html);

                    },
                    error: function () {
                        alert(html);
                        $("body").isLoading("hide");
                    },
                });
                return false;
            }
        });
        $('#sendForm').on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'pots/importExcel/';
                var data = new FormData(this);
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $("body").isLoading({
                            text: "",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                        });
                    },
                    success: function (html) {
                        setTimeout(function () {
                            scrollTo();
                            $('#modalImport').modal('hide');
                            notify('Data berhasil disimpan!', 'success');
                        }, 500);
                        $("body").isLoading("hide");
                        $('#modalImport').modal("hide");
                        location.reload(true);
                    },
                    error: function () {
                        notify('Data gagal disimpan!', 'warning');
                        $('#modalImport').modal('hide');
                        $("body").isLoading("hide");
                    },
                });
                return false;
            }
        });
        $('#sendFormManual').on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'pots/updateData/' + $('#kode').val();
                var data = $("#sendFormManual").serialize();
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    beforeSend: function () {
                        $("body").isLoading({
                            text: "",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                        });
                    },
                    success: function (html) {
                        setTimeout(function () {
                            scrollTo();
                            $('#modalNgisi').modal('hide');
                            notify('Data berhasil disimpan!', 'success');
                        }, 500);
                        $("body").isLoading("hide");
                        $('#modalNgisi').modal("hide");
                        location.reload(true);
                    },
                    error: function () {
                        notify('Data gagal disimpan!', 'warning');
                        $('#modalNgisi').modal('hide');
                        $("body").isLoading("hide");
                    },
                });
                return false;
            }
        });
    });
    function check(html) {
        var txt;
        var r = confirm(html);
        if (r == true) {
            $.ajax({
                sync: true,
                url: "telegaul/kirimData",
                data: $("#sendFormCheck").serialize(),
                type: 'POST',
                datatype: 'html',
                beforeSend: function (data) {
                    $("body").isLoading({
                        text: "",
                        position: "overlay",
                        tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                    });
                },
                success: function (html2) {
                    alert(html2);
                    $("body").isLoading("hide");
                },
                error: function () {
                    alert(html);
                    $("body").isLoading("hide");
                },
            });
            $("body").isLoading("hide");
        } else {
            $("body").isLoading("hide");
        }
    }
</script>


