<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
<!--    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Pencarian Data</h3>
                </div>
                <div class="box-body">
                    <form role="form" id="formcari" enctype="multipart/form-data" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-4 label-control" for="exampleInputFile">Service ID</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="service_id">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" id="cari">Cari</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>-->
    <div class="row" id="rowtabel">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">List Data</h3>
                </div>
                <div class="box-body">
                    <table id="data-datin-cari" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <?php
                                if ($kolom) {
                                    foreach ($kolom as $key => $value) {
                                        if (strlen($value) == 0) {
                                            echo '<th data-type="numeric"></th>';
                                        } else {
                                            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                        }
                                    }
                                }
                                ?>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <?php
                                if ($kolom) {
                                    foreach ($kolom as $key => $value) {
                                        if (strlen($value) == 0) {
                                            echo '<th data-type="numeric"></th>';
                                        } else {
                                            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                        }
                                    }
                                }
                                ?>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($rowdata as $row): ?>
                                <tr>
                                    <td><?= $row->kode; ?></td>
                                    <td><?= $row->service_id; ?></td>
                                    <td><?= $row->top_priority; ?></td>
                                    <td><?= $row->nama_tech; ?></td>
                                    <td><?= $row->actual_solution; ?></td>
                                    <td><?= $row->compliance; ?></td>
                                    <td><?= $row->nama_problem; ?></td>
                                    <td><?= $row->nama_act; ?></td>
                                    <td>
<!--                                        <button id="btnNgisi" class="btn btn-warning btn-md btnNgisi" data-id="<?= $row->kode; ?>"><i class="fa fa-edit"></i></button>-->
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>