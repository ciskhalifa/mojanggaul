<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Pots extends MX_Controller {

    public function __construct() {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index() {
        $data['content'] = 'pots';
        $data['css'] = 'css';
        $data['js'] = 'js';
        $data['kolom'] = array("Incident", "Service ID", "Top Priority", "Technology", "Actual Solution", "Compliance", "Problem", "GROUP Act Solution", "Opsi");
        $sTable = 'vpots';
        $sWhere = "";
        $tQuery = "SELECT * FROM (SELECT @row := @row + 1 AS no, $sTable.*, '' as opsi FROM $sTable, (SELECT @row := 0) AS r) AS tab WHERE 1=1 $sWhere ";
        $data['rowdata'] = $this->Data_model->jalankanQuery($tQuery, 3);
        $this->load->view('default', $data);
    }

    public function cekDataClose() {
        $kode = $this->input->post('service_id');
        $in = $this->input->post('notik');
        $q = $this->Data_model->jalankanQuery("SELECT service_id, count(*) as countdata FROM t_datin WHERE service_id ='" . $kode . "'", 3);
        if ($q[0]->countdata == 0) {
            $totaldata = $q[0]->countdata + 1;
            $data = array(
                'notik' => $this->input->post('notik'),
                'service_id' => $this->input->post('service_id'),
                'kode_witel' => $this->input->post('kode_witel')
            );
            $this->Data_model->simpanData($data, 't_datin_gaul');
            $this->BroadcastGaul($totaldata, $in, $kode);
        } else {
            $totalgaul = $q[0]->countdata + 1;
            $data = array(
                'notik' => $this->input->post('notik'),
                'service_id' => $this->input->post('service_id'),
                'kode_witel' => $this->input->post('kode_witel')
            );
            $this->Data_model->simpanData($data, 't_datin_gaul');
            $this->BroadcastGaul($totalgaul, $in, $kode);
        }
    }

    public function BroadcastGaul($totaldata, $kode, $sid = "") {
        // -273307413     -> CCAN Bandung Barat 
        // ?      -> CCAN WITEL BANDUNG
        // ?      -> CCAN REBORN KARAWANG
        // -31861253      -> CCAN FIBER CIREBON
        // -1001103012740 -> CCAN & ROC BGES
        // -1001106952614 -> CCAN JABSEL
        // -1001075924157 -> GROUP CCAN ASSURANCE TSM
        // -127314376     -> CCAN Copper Adv Cirebon,Indramayu,Majalengka,Kuningan
        // 332908516      -> DZIK
        // 541347757      -> CIS
        // 170952523      -> Teh Isti

        $token = "488307944:AAFfGgrVfv2BzScmODFkQ0VKiAaqNvj1KpI";
        $chat_id = array("-273307413", "-31861253", "-1001103012740", "-1001106952614", "-1001075924157", "-127314376", "332908516");
//        $chat_id = array("332908516", "541347757", "170952523");
        if ($totaldata > 1) {
            $text = "No. Tiket = " . $kode . "\nService ID = " . $sid . "\nGangguan Ke-" . $totaldata;
            $text .= "\nMohon Perhatian Dan Pengawalannya";
//            $text .= "\nTes BOT MojangGaul";
        } else {
            $text = "No. Tiket =" . $kode . "\nService ID = " . $sid . "\nGangguan Ke-" . $totaldata;
            $text .= "\nMohon Perhatian Dan Pengawalannya";
//            $text .= "\nTes BOT MojangGaul";
        }
        foreach ($chat_id as $row) {
            $url = "https://api.telegram.org/bot" . $token . "/";
            $url .= "sendMessage?chat_id=" . $row . "&text=" . urlencode($text);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, NULL);
            $output = curl_exec($ch);
        }
        curl_close($ch);
        echo "Berhasil Di kirim";
    }

    public function updateData() {
        $kode = $this->uri->segment(3);
        $tabel = 't_datin';
        $kondisi = array('kode' => $kode);
        if (IS_AJAX) {
            $arrdata = array();
            foreach ($this->input->post() as $key => $value) {
                $arrdata[$key] = $value;
            }
            $this->Data_model->updateDataWhere($arrdata, $tabel, $kondisi);
        }
    }

    public function importExcel() {
        $basepath = BASEPATH;
        date_default_timezone_set("Asia/Bangkok");
        $date = date("Y-m-d H:i:s");
        $stringreplace = str_replace("system", "publik/", $basepath);
        $config['upload_path'] = $stringreplace;
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $files = $_FILES;
        $_FILES['userfile']['name'] = $files['file']['name'];
        $_FILES['userfile']['type'] = $files['file']['type'];
        $_FILES['userfile']['tmp_name'] = $files['file']['tmp_name'];
        $_FILES['userfile']['error'] = $files['file']['error'];
        $_FILES['userfile']['size'] = $files['file']['size'];
        $new_name = $files['file']['name'];
        $config['file_name'] = $new_name;
        //print_r($config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload()) {
            $this->prosesXLS($new_name);
        } else {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        }
    }

    function prosesXLS($namafile) {
        error_reporting(E_ALL & ~E_NOTICE);
        $urutankode = 1;
        $nama = str_replace(" ", "_", $namafile);
        $file = 'publik/' . $nama;
        /*
         * load the excel library
         */
        $this->load->library('excel');
        /*
         * read file from path
         */
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        /*
         * get only the Cell Collection
         */
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        /*
         * extract to a PHP readable array format
         */

        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            $data_type = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();

            /*
             * header will/should be in row 1 only. of course this can be modified to suit your need.
             */
            if ($row <= 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }

        $kolom = array("kode", "customer_name", "contact_name", "contact_phone", "contact_email", "summary", "owner_group", "owner", "last_workupdate_log", "last_work_log_date", "count_custinfo",
            "last_custinfo", "assigned_to", "assigned_by", "reported_priorty", "source", "ext_tic_id", "ext_tic_stat", "segment", "channel", "customer_segment", "customer_id", "service_id",
            "service_no", "service_type", "slg", "top_priority", "kode_tech", "datek", "rk_name", "induk_gamas", "reported_date", "ttr_customer", "ttr_nasional",
            "ttr_regional", "ttr_witel", "ttr_mitra", "ttr_agent", "ttr_pending", "status", "hasil_ukur", "osm_resolved", "last_update_tic", "status_date", "closed_time", "resolved_by",
            "kode_workzone", "witel", "regional", "incident_sym", "solution_segment", "actual_solution");

        foreach ($arr_data as $row):
            if ($this->cekIsiData($row, $baris) == true) :
                $kode = intval($kl) . $urutankode;
                /*
                 * $x := nilai untuk array $kolom
                 * $k := nilai untuk array xls
                 */
                $x = 0;
                $k = 1;
                $i = 1;
                $data = array();
                $data2 = array();
                foreach ($row as $col):
                    if (substr($col, 0, 1) == "=") {
                        $nilai = "";
                    } else {
                        $nilai = ($col == '') ? NULL : $col;
                    }
                    switch ($x):
                        case 0:
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM t_datin WHERE kode ='$nilai' LIMIT 1", 1);
                            //print_r($carikode);
                            if (empty($carikode)) {
                                $data['kode'] = $nilai;
                                $data['flag'] = 0;
                            } else {
                                $data['flag'] = 1;
                                $data['kode'] = $nilai;
                            }
                            break;
                        case 26:
                            $pisah = explode(' ', $nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_technology WHERE nama_tech LIKE '%$pisah[0]%' LIMIT 1", 1);
                            $data['kode_tech'] = ($carikode) ? $carikode->kode : 0;
                            break;
                        case 45:
                            $pisah = explode(' ', $nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_workzone WHERE nama_workzone LIKE '%$pisah[0]%' LIMIT 1", 1);
                            $data['kode_workzone'] = ($carikode) ? $carikode->kode : 0;
                            break;
                        default :
                            $data[$kolom[$x]] = $nilai;
                            break;
                    endswitch;
                    $k++;
                    $x++;

                endforeach;
                $i++;
                $data['week'] = $this->input->post('week');
                try {
                   // echo '<pre>';
                   // print_r($data);
                   // echo '</pre>';
                   //die;
                    if ($data['flag'] == 1) {
                        $kondisi = array('kode' => $data['kode']);
                        $flag = array('flag' => $data['flag']);
                        $this->Data_model->updateDataWhere($data, 't_pots', $kondisi);
                        //echo $this->db->last_query();
                    } else {
                        $this->Data_model->simpanData($data, 't_pots');
                        //echo $this->db->last_query();
                    }
                } catch (Exception $exc) {
                    $data['infoerror'] = $exc->getMessage();
                }
            else:

            endif;

        endforeach;
    }

    function cekIsiData($row, $baris) {
        $isi = false;
        foreach ($row as $eusi) {
            $isi = (strlen(trim($eusi)) > 0) ? true : (($isi == true) ? $isi : false);
        }
        return $isi;
    }

}
