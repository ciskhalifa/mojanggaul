<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<div id="myConfirm" class="modal fade">
    <div class="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <p>Apakah anda akan melakukan <span class="lblModal h4"></span> ?</p>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="cid"><input type="hidden" id="cod"><input type="hidden" id="getto">
                    <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" id="btnYes" class="btn btn-danger">Hapus</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/bower_components/select2/dist/css/select2.min.css'); ?>">
<!-- Select2 -->
<script src="<?= base_url('assets/bower_components/select2/dist/js/select2.full.min.js'); ?>"></script>
<script>
    var myApp = myApp || {};
    var p;
    $(function () {
        $(".menuitem").bind('click', function () {
            if (!$(this).hasClass('active')) {
                $('.menuitem').removeClass('active');
                $(this).addClass('active');
                $('.jdlmaster').text($(this).attr('data-default').replace("_", " "));
                $.ajax({
                    type: 'POST',
                    url: 'master/loadHalaman/' + $(this).attr('data-default'),
                    success: function (result) {
                        if (result.length == 0) {
                            myApp.oTable.fnDestroy();
                            $("#divhalaman").html('');
                        } else {
                            $("#divhalaman").html(result);
                        }
                        scrollTo();
                    }
                });
            }
        });
        $("#btnYes").bind("click", function () {
            var link = $("#getto").val();
            $.ajax({url: link, type: "POST", data: "cid=" + $("#cid").val() + "&cod=" + $("#tabel").val(), dataType: "html", beforeSend: function () {
                    if (link != "#") {
                    }
                }, success: function (html) {
                    myApp.oTable.fnDraw(false);
                    notify("Delete berhasil", "danger")
                    $("#myConfirm").modal("hide")
                }})
        });
    });
</script>


