<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai):
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = ($aep == 'salin') ? '' : $arey['kode'];
}else {
    $cid = '';
}
?>
<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group">
            <label class="col-md-2 label-control">Nama Problem</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Nama Problem" name="nama_problem" id="nama_problem" value="<?= (isset($arey)) ? $arey['nama_problem'] : ''; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="box-footer">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function () {
        $("#tmblBatal").on("click", function () {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function (c) {
            if (c.isDefaultPrevented()) {
            } else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = $("#xfrm").serialize();
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    dataType: "html",
                    beforeSend: function () {
                        $(".box #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                        })
                    },
                    success: function (d) {
                        setTimeout(function () {
                            $(".box #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function () {
                        setTimeout(function () {
                            $(".box #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>
