<?php
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
if (isset($rowdata)) {
    $arey = array();
    foreach ($rowdata as $kolom => $nilai):
        $arey[$kolom] = $nilai;
    endforeach;
    $cid = ($aep == 'salin') ? '' : $arey['kode'];
}else {
    $cid = '';
}
?>
<form role="form" id="xfrm" enctype="multipart/form-data" class="form form-horizontal">
    <div class="form-body">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cid; ?>">
        <div class="form-group row">
            <label class="col-md-2 label-control">Nama</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Nama" name="nama" id="nama" value="<?= (isset($arey)) ? $arey['nama'] : ''; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Email</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Email" name="email" id="email" value="<?= (isset($arey)) ? $arey['email'] : ''; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Username</label>
            <div class="col-md-4">
                <input type="text" class="form-control input-sm" placeholder="Username" name="username" id="username" value="<?= (isset($arey)) ? $arey['username'] : ''; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Password</label>
            <div class="col-md-4">
                <input type="password" class="form-control input-sm" placeholder="Password" name="password" id="password" value="<?= (isset($arey)) ? $arey['password'] : ''; ?>" data-error="wajib diisi" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-2 label-control">Role</label>
            <div class="col-md-4">
                <select class="select2 form-control" name="role" id="role">
                    <option value="">- Pilihan -</option>
                    <?php  $n = (isset($arey)) ? $arey['role'] : ''; ?>
                    <option value="1" <?= ($n == 1) ? " selected= selected" : "" ?>> Administrator </option>
                    <option value="2" <?= ($n == 2) ? " selected= selected" : "" ?>> Users </option>
                </select>
            </div>
        </div>
        <div class="form-actions">
            <button class="btn btn-primary"><i class="icon-check2"></i> Simpan</button>
            <a href="javascript:" class="btn btn-warning" id="tmblBatal"><i class="icon-cross2"></i> Batal</a>
        </div>
    </div>
</form>
<script>
    $(function () {
    
        $(".select2").select2();
        $("#tmblBatal").on("click", function () {
            $("#divdua").slideUp();
            $("#divsatu").slideDown();
            $("#divform").html("");
        });
        $("#xfrm").on("submit", function (c) {
            if (c.isDefaultPrevented()) {
            } else {
                var b = "master/simpanData/" + $("#tabel").val();
                var a = $("#xfrm").serialize();
                $.ajax({
                    url: b,
                    type: "POST",
                    data: a,
                    dataType: "html",
                    beforeSend: function () {
                        $(".box #divform").isLoading({
                            text: "Proses Simpan",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"></circle></svg></div>'
                        })
                    },
                    success: function (d) {
                        setTimeout(function () {
                            $(".box #divform").isLoading("hide");
                            myApp.oTable.fnDraw(false);
                            $("#divdua").slideUp();
                            $("#divsatu").slideDown();
                            notify("Penyimpanan berhasil", "success")
                        }, 1000)
                    },
                    error: function () {
                        setTimeout(function () {
                            $(".box #divform").isLoading("hide")
                        }, 1000)
                    }
                });
                return false
            }
            return false
        })
    }); /*]]>*/
</script>