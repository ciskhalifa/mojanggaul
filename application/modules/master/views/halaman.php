<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<div id="divsatu">
    <input type="hidden" id="tabel" value="<?php echo $tabel; ?>">
    <input type="hidden" id="kolom" value="<?php echo $jmlkolom; ?>">
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= strtoupper($this->uri->segment('3')); ?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" id="tmblTambah"><i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div class="box-body">
                <div class="table-responsive mailbox-messages">
                    <table id="data-table-basic" class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <?php
                                if ($kolom) {
                                    foreach ($kolom as $key => $value) {
                                        if (strlen($value) == 0) {
                                            echo '<th data-type="numeric"></th>';
                                        } else {
                                            echo '<th data-column-id="' . $key . '" data-type="numeric">' . $value . '</th>';
                                        }
                                    }
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divdua" class="content-detached content-right" style="display:none">
    <div class="col-md-9">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Form <?= strtoupper($this->uri->segment('3')); ?></h3>
            </div>
            <div class="box-body">
                <div class="card-body card-padding" id="divform"></div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/jquery.isloading.min.js'); ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script>
    $(function () {
        var myApp = myApp || {};
        if ($("#kolom").val() > 0) {
            loadTabelGlobalCIS($("#kolom").val(), "master", "");
        }
        $("#tmblTambah").on("click", function () {
            $("#divform").load("master/loadForm/" + $("#tabel").val() + "/-/");
            $("#divsatu").slideUp('fast');
            $("#divdua").slideDown('fast');
        });
        $("#tmblBatal").on("click", function () {
            $("#divdua").slideUp('fast');
            $("#divsatu").slideDown('fast');
            myApp.oTable.fnDraw(false);

        });
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>

