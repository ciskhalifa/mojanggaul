<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Master Data</h3>
                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="nav nav-pills nav-stacked">
                        <!--<li class="menuitem"data-default="customer"><a href="javascript:;"><i class="fa fa-users"></i> Customer</a></li>-->
                        <li class="menuitem" data-default="technology"><a href="javascript:;"><i class="fa fa-cogs"></i> Technology</a></li>
                        <li class="menuitem" data-default="problem"><a href="javascript:;"><i class="fa fa-gears"></i> Problem</a></li>
                        <li class="menuitem" data-default="workzone"><a href="javascript:;"><i class="fa fa-building"></i> Workzone</a></li>
                        <li class="menuitem" data-default="groupact"><a href="javascript:;"><i class="fa fa-users"></i> Grouping Act Solution</a></li>
                        <li class="menuitem" data-default="witel"><a href="javascript:;"><i class="fa fa-building"></i> Witel</a></li>
                        <li class="menuitem" data-default="user"><a href="javascript:;"><i class="fa fa-user"></i> Users</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="divhalaman"></div>
    </div>

</section>

