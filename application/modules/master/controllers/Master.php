<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Master extends MX_Controller {

    public function __construct() {
        parent::__construct();
        if (!isset($_SESSION['username'])) {
            redirect('../login');
        }
    }

    public function index() {
        $data['content'] = 'master';
        $data['js'] = 'js';
        $data['css'] = 'css';

        $this->load->view('default', $data);
    }

    function loadHalaman() {
        $konten['tabel'] = $this->uri->segment(3);
        switch ($this->uri->segment(3)) :
            case 'technology':
                $konten['kolom'] = array("Kode", "Nama Technology", "");
                break;
            case 'problem':
                $konten['kolom'] = array("Kode", "Nama Problem", "");
                break;
            case 'workzone':
                $konten['kolom'] = array("Kode", "Nama Workzone", "");
                break;
            case 'groupact':
                $konten['kolom'] = array("Kode", "Nama Act Solution", "");
                break;
            case 'witel':
                $konten['kolom'] = array("Kode", "Nama Witel", "");
                break;
            case 'user':
                $konten['kolom'] = array("Kode", "Nama", "Email", "Username", "Role", "");
                break;
            default :
                $konten['kolom'] = "";
                break;
        endswitch;

        $konten['jmlkolom'] = count($konten['kolom']);
        $this->load->view('halaman', $konten);
    }

    function getData() {
        /*
         * list data 
         */
        if (IS_AJAX) {
            $sTablex = "";
            $order = "";
            $sTable = 'm_' . $this->uri->segment(3);
            $k = '';
            switch ($this->uri->segment(3)) :
                case 'technology':
                    $aColumns = array("kode", "nama_tech", "opsi");
                    $kolom = "kode, nama_tech";
                    $sIndexColumn = "kode";
                    break;
                case 'problem':
                    $aColumns = array("kode", "nama_problem", "opsi");
                    $kolom = "kode, nama_problem";
                    $sIndexColumn = "kode";
                    break;
                case 'workzone':
                    $aColumns = array("kode", "nama_workzone", "opsi");
                    $kolom = "kode, nama_workzone";
                    $sIndexColumn = "kode";
                    break;
                case 'groupact':
                    $aColumns = array("kode", "nama_act", "opsi");
                    $kolom = "kode, nama_act";
                    $sIndexColumn = "kode";
                    break;
                case 'witel':
                    $aColumns = array("kode", "nama_witel", "opsi");
                    $kolom = "kode, nama_witel";
                    $sIndexColumn = "kode";
                    break;
                case 'user':
                    $aColumns = array("kode", "nama", "email", "username", "role", "opsi");
                    $kolom = "kode, nama, email, username, IF(role=1, 'Administrator', 'User') as role";
                    $sIndexColumn = "kode";
                    break;
                default :
                    $sTable = 'm_perusahaan';
                    $aColumns = array("kode", "nama_perusahaan", "opsi");
                    $kolom = "kode, nama_perusahaan";
                    $sIndexColumn = "kode";
                    break;
            endswitch;
            if (isset($kolom) && strlen($kolom) > 0) {
                $where = "";
                $tQuery = "SELECT $kolom ,'$k' AS opsi  "
                        . "FROM $sTable a $sTablex $where WHERE 1=1";
                echo $this->libglobal->pagingData($aColumns, $sIndexColumn, $sTable, $tQuery, $sTablex);
            } else {
                echo "";
            }
        }
    }

    function loadForm() {
        $konten['aep'] = $this->uri->segment(5);
        if ($this->uri->segment(4) <> '-') {
            $nilai = $this->uri->segment(4);
            if (strtoupper($this->uri->segment(5)) == 'SALIN') {
                if ($this->uri->segment(6) == '' || strlen(trim($this->uri->segment(6))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(6))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(6)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            } else {
                if ($this->uri->segment(5) == '' || strlen(trim($this->uri->segment(5))) > 1) {
                    if (strpos($nilai, '-')) {
                        $exp = explode("-", $nilai);
                        $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                    } else {
                        $kondisi = array('kode' => $nilai);
                    }
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3));
                } else {
                    $tabel = 'm_' . ((strlen(trim($this->uri->segment(5))) > 1) ? $this->uri->segment(3) : $this->uri->segment(3) . '_' . strtolower($this->uri->segment(5)));
                    $exp = explode("-", $nilai);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                }
            }
            $konten['rowdata'] = $this->Data_model->satuData($tabel, $kondisi);
        }
        $this->load->view('form/' . $this->uri->segment(3), $konten);
    }

    function simpanData() {
        $arrdata = array();
        $cid = '';
        $tabel = 'm_' . $this->uri->segment(3);
        foreach ($this->input->post() as $key => $value) {
            if (is_array($value)) {
                
            } else {
                $subject = strtolower($key);
                $pattern = '/tgl/i';
                if ($key == 'cid') {
                    $cid = $value;
                } else {
                    if (preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE)) {
                        if (strlen(trim($value)) > 0) {
                            $tgl = explode("/", $value);
                            $newtgl = $tgl[1] . "/" . $tgl[0] . "/" . $tgl[2];
                            $time = strtotime($newtgl);
                            $arrdata[$key] = date('Y-m-d', $time);
                        } else {
                            $arrdata[$key] = NULL;
                        }
                    } else {
                        if ($this->uri->segment(4) == "user") {
                            $arrdata['password'] = md5($this->input->post('password'));
                            $arrdata[$key] = $value;
                        }
                        $arrdata[$key] = $value;
                    }
                }
            }
        }
        if ($cid == "") {
            $arrdata['user_data'] = $_SESSION['kode'];
            try {
                $this->Data_model->simpanData($arrdata, $tabel);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            $kondisi = 'kode';
            $arrdata['tgl_modify'] = date('Y-m-d H:i:s');
            $arrdata['user_modify'] = $_SESSION['kode'];
            try {
                $kondisi = array('kode' => $cid);
                $this->Data_model->updateDataWhere($arrdata, $tabel, $kondisi);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
    }

    function hapus() {
        if (IS_AJAX) {
            $param = $this->input->post('cid');
            if ($this->input->post('cid') != '') {
                if (strpos($param, '-')) {
                    $exp = explode("-", $param);
                    $kondisi = array('kode' => $exp[0], 'seri' => $exp[1]);
                } else {
                    $kondisi = array('kode' => $param);
                }
                $this->Data_model->hapusDataWhere('m_' . $this->input->post('cod'), $kondisi);
                echo json_encode("ok");
            }
        }
    }

}
