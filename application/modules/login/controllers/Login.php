<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

class Login extends MX_Controller {

    function __construct() {
        parent::__construct();
        
    }

    public function index() {
        if (isset($_SESSION['username'])) {
            redirect('../datin');
        }
        $data['js'] = 'js';
        $data['css'] = 'css';
        $data['content'] = 'login';

        $this->load->view('layout_login', $data);
    }

    public function doLogin() {
        if (IS_AJAX) {
            $res = $this->Data_model->verify_user(array('username' => $this->input->post('username'), 'password' => md5($this->input->post('password'))), 'm_user');
            //echo $this->db->last_query();
            if ($res !== FALSE) {
                foreach ($res as $row => $kolom) {
                    $_SESSION[$row] = $kolom;
                }
                echo base_url('datin');
            } else {
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">*</span></button> Password / Username tidak ditemukan/salah.';
            }
        }
    }

    public function doOut() {
        session_unset();
        session_destroy();
        $this->index();
    }

}
