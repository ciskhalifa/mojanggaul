<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>Mojang Gaul</b></a>
    </div>
    <div class="register-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <div class="alert alert-danger alert-dismissible" role="alert" style="display: none;"></div>
        <form class="form-horizontal form-simple" id="xfrm" method="POST" role="form">
            <div class="form-group has-feedback">
                <input type="text" class="form-control" placeholder="Username" name="username" id="user-name">
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                <span class="fa fa-unlock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
        </form>
        <a href="#">I forgot my password</a><br>
    </div>
</div>
