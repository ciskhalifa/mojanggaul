<?php

/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Telegaul extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $data['content'] = 'telegaul';
        $data['css'] = 'css';
        $data['js'] = 'js';
        $this->load->view('defaultcek', $data);
    }

    public function cekDataClose() {
        $kode = $this->input->post('service_id');
        $in = $this->input->post('notik');
        $kode_witel = $this->input->post('kode_witel');
        $type = $this->input->post('type');
        $date = date('m');
        if ($type == "datin") {
            $q = $this->Data_model->jalankanQuery("SELECT kode, service_id, count(*) as countdata FROM t_datin WHERE service_id ='" . $kode . "'", 3);
            $a = $this->Data_model->jalankanQuery("SELECT kode FROM t_datin WHERE service_id ='" . $kode . "'", 3);
        } else {
            $q = $this->Data_model->jalankanQuery("SELECT kode, service_id, count(*) as countdata FROM t_pots WHERE service_id ='" . $kode . "'", 3);
            $a = $this->Data_model->jalankanQuery("SELECT kode FROM t_pots WHERE service_id ='" . $kode . "'", 3);
        }

        if ($q[0]->countdata == 0) {
            if (empty($koderef)) {
                $koderef = "Tidak Ada";
            } else {
                $koderef = $a[0];
            }
            $totaldata = $q[0]->countdata + 1;
            $data = array(
                'notik' => $this->input->post('notik'),
                'service_id' => $this->input->post('service_id'),
                'kode_witel' => $this->input->post('kode_witel')
            );
            $text = "No. Tiket = " . $in . "\nService ID = " . $kode . "\nGangguan Ke-" . $totaldata;
            $text .= "\nTiket Referensi : " . $koderef;
            $text .= "\n\nMohon Perhatian Dan Pengawalannya";
//            $text .= "\n\nApakah Anda yakin ingin memproses SID ini ? ";
            echo $text;
        } else {
            $totalgaul = $q[0]->countdata + 1;
            $data = array(
                'notik' => $this->input->post('notik'),
                'service_id' => $this->input->post('service_id'),
                'kode_witel' => $this->input->post('kode_witel')
            );
            $text = "No. Tiket = " . $in . "\nService ID = " . $kode . "\nGangguan Ke-" . $totalgaul;
            $text .= "\nTiket Referensi : \n";
            for ($s = 0; $s < count($a); $s++) {
                $koderef = $a[$s];
                $text .= $koderef->kode . "\n";
            }
            $text .= "\nMohon Perhatian Dan Pengawalannya";
//            $text .= "\n\nApakah Anda yakin ingin memproses SID ini ? ";
            echo $text;
        }
    }
    public function cekDataClose2($kode, $in, $kode_witel, $type) {
        $setring = str_replace( , "", $kode_witel);
        if ($setring == "BANDUNGBARAT") {
            $witel = 1;
        } else if ($setring == "JABARSELATAN(SUKABUMI)") {
            $witel = 2;
        } else if ($setring == "JABARTENGAH(BANDUNG)") {
            $witel = 3;
        } else if ($setring == "JABARTIMSEL(TASIKMALAYA)") {
            $witel = 4;
        } else if ($setring == "JABARTIMUR(CIREBON)") {
            $witel = 5;
        } else if ($setring == "JABARUTARA(KARAWANG)") {
            $witel = 6;
        } else {
            $witel = "KOSONG";
        }
        echo '<pre>';
        print_r($witel);
        echo '</pre>';
        if ($type == "datin") {
            $q = $this->Data_model->jalankanQuery("SELECT kode, service_id, count(*) as countdata FROM t_datin WHERE service_id ='" . $kode . "'", 3);
            $a = $this->Data_model->jalankanQuery("SELECT kode FROM t_datin WHERE service_id ='" . $kode . "'", 3);
        } else {
            $q = $this->Data_model->jalankanQuery("SELECT kode, service_id, count(*) as countdata FROM t_pots WHERE service_id ='" . $kode . "'", 3);
            $a = $this->Data_model->jalankanQuery("SELECT kode FROM t_pots WHERE service_id ='" . $kode . "'", 3);
        }

        if ($q[0]->countdata == 0) {
            if (empty($koderef)) {
                $koderef = "Tidak Ada";
            } else {
                $koderef = $a[0];
            }
            $totaldata = $q[0]->countdata + 1;
            $data = array(
                'notik' => $in,
                'service_id' => $kode,
                'kode_witel' => $witel
            );

            $text = "No. Tiket = " . $in . "\nService ID = " . $kode . "\nGangguan Ke-" . $totaldata;
            $text .= "\nTiket Referensi : " . $koderef;
            $text .= "\n\nMohon Perhatian Dan Pengawalannya";
            //$this->BroadcastGaul($totaldata, $in, $kode, $witel, $text);
        } else {
            $totalgaul = $q[0]->countdata + 1;
            $data = array(
                'notik' => $in,
                'service_id' => $kode,
                'kode_witel' => $witel
            );

            $text = "No. Tiket = " . $in . "\nService ID = " . $kode . "\nGangguan Ke-" . $totalgaul;
            $text .= "\nTiket Referensi : \n";
            for ($s = 0; $s < count($a); $s++) {
                $koderef = $a[$s];
                $text .= $koderef->kode . "\n";
            }
            $text .= "\nMohon Perhatian Dan Pengawalannya";
            //$this->BroadcastGaul($totalgaul, $in, $kode, $witel, $text);
        }
    }
    public function kirimData() {
        $kode = $this->input->post('service_id');
        $in = $this->input->post('notik');
        $kode_witel = $this->input->post('kode_witel');
        $text = $this->input->post('textmen');
        $type = $this->input->post('type');
        if ($type == "datin") {
            $q = $this->Data_model->jalankanQuery("SELECT service_id, count(*) as countdata FROM t_datin WHERE service_id ='" . $kode . "'", 3);
        } else {
            $q = $this->Data_model->jalankanQuery("SELECT service_id, count(*) as countdata FROM t_pots WHERE service_id ='" . $kode . "'", 3);
        }
        
        if ($q[0]->countdata == 0) {
            $totaldata = $q[0]->countdata + 1;
            $data = array(
                'notik' => $this->input->post('notik'),
                'service_id' => $this->input->post('service_id'),
                'kode_witel' => $this->input->post('kode_witel')
            );
            if ($type == "datin") {
                $this->Data_model->simpanData($data, 't_datin_gaul');
            } else {
                $this->Data_model->simpanData($data, 't_pots_gaul');
            }
            //$this->BroadcastGaul($totaldata, $in, $kode, $kode_witel, $text);
        } else {
            $totalgaul = $q[0]->countdata + 1;
            $data = array(
                'notik' => $this->input->post('notik'),
                'service_id' => $this->input->post('service_id'),
                'kode_witel' => $this->input->post('kode_witel')
            );
            if ($type == "datin") {
                $this->Data_model->simpanData($data, 't_datin_gaul');
            } else {
                $this->Data_model->simpanData($data, 't_pots_gaul');
            }
            $this->BroadcastGaul($totalgaul, $in, $kode, $kode_witel, $text);
        }
    }

    public function BroadcastGaul($totaldata, $kode, $sid = "", $kode_witel = "", $text) {
        // -273307413     -> CCAN Bandung Barat 
        // -1001087167966 -> CCAN WITEL BANDUNG
        // -137495720     -> CCAN REBORN KARAWANG
        // -31861253      -> CCAN FIBER CIREBON
        // -1001103012740 -> CCAN & ROC BGES
        // -1001106952614 -> CCAN JABSEL
        // -1001075924157 -> GROUP CCAN ASSURANCE TSM
        // -127314376     -> CCAN Copper Adv Cirebon,Indramayu,Majalengka,Kuningan
        // 332908516      -> DZIK
        // 541347757      -> CIS
        // 170952523      -> Teh Isti
        $token = "488307944:AAFfGgrVfv2BzScmODFkQ0VKiAaqNvj1KpI";
        if ($kode_witel == 1) { // BANDUNG BARAT
            $chat_id = "-273307413";
        } else if ($kode_witel == 2) { // SUKABUMI
            $chat_id = "-1001106952614";
        } else if ($kode_witel == 3) { // BANDUNG
            $chat_id = array("-1001087167966", "-1001103012740");
        } else if ($kode_witel == 4) { // TASIKMALAYA
            $chat_id = "-1001075924157";
        } else if ($kode_witel == 5) { // CIREBON
            $chat_id = array("-31861253", "-127314376");
        } else if ($kode_witel == 6) { // KARAWANG
            $chat_id = "-137495720";
        }
        // if ($kode_witel == 1) { // BANDUNG BARAT
        //     $chat_id = "170952523";
        //     echo $chat_id;
        // } else if ($kode_witel == 3) { // BANDUNG
        //     $chat_id = "332908516";
        //     echo $chat_id;
        // } else if ($kode_witel == 6) { // KARAWANG
        //     $chat_id = "541347757";
        //     echo $chat_id;
        // }
        //die;
        //$chat_id = array("-273307413", "-31861253", "-1001103012740", "-1001106952614", "-1001075924157", "-127314376", "332908516");
        // $chat_id = array("332908516", "541347757", "170952523");
        if (is_array($chat_id)) {
            foreach ($chat_id as $row) {
                $url = "https://api.telegram.org/bot" . $token . "/";
                $url .= "sendMessage?chat_id=" . $row . "&text=" . urlencode($text);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, NULL);
                $output = curl_exec($ch);
            }
            curl_close($ch);
            echo "Berhasil Di kirim";
        } else {
            $url = "https://api.telegram.org/bot" . $token . "/";
            $url .= "sendMessage?chat_id=" . $chat_id . "&text=" . urlencode($text);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, NULL);
            $output = curl_exec($ch);
            curl_close($ch);
            echo "Berhasil Di kirim";
        }
    }

    public function importExcel() {
        $basepath = BASEPATH;
        date_default_timezone_set("Asia/Bangkok");
        $date = date("Y-m-d H:i:s");
        $stringreplace = str_replace("system", "publik/", $basepath);
        $config['upload_path'] = $stringreplace;
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        $files = $_FILES;
        $_FILES['userfile']['name'] = $files['file']['name'];
        $_FILES['userfile']['type'] = $files['file']['type'];
        $_FILES['userfile']['tmp_name'] = $files['file']['tmp_name'];
        $_FILES['userfile']['error'] = $files['file']['error'];
        $_FILES['userfile']['size'] = $files['file']['size'];
        $new_name = $files['file']['name'];
        $config['file_name'] = $new_name;
        //print_r($config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload()) {
            $this->prosesXLS($new_name);
        } else {
            $error = array('error' => $this->upload->display_errors());
            print_r($error);
        }
    }

    function prosesXLS($namafile) {
        $type = $this->input->post('type');
        error_reporting(E_ALL & ~E_NOTICE);
        $urutankode = 1;
        $nama = str_replace(" ", "_", $namafile);
        $file = 'publik/' . $nama;
        /*
         * load the excel library
         */
        $this->load->library('excel');
        /*
         * read file from path
         */
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        /*
         * get only the Cell Collection
         */
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        /*
         * extract to a PHP readable array format
         */

        foreach ($cell_collection as $cell) {
            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            $data_type = $objPHPExcel->getActiveSheet()->getCell($cell)->getFormattedValue();

            /*
             * header will/should be in row 1 only. of course this can be modified to suit your need.
             */
            if ($row <= 1) {
                $header[$row][$column] = $data_value;
            } else {
                $arr_data[$row][$column] = $data_value;
            }
        }

        $kolom = array("kode", "customer_name", "summary", "owner_group", "owner", "last_workupdate_log", "last_work_log_date", "count_custinfo",
            "last_custinfo", "assigned_to", "assigned_by", "source", "ext_tic_id", "ext_tic_stat", "segment", "channel", "customer_segment", "service_id",
            "service_no", "service_type", "top_priority", "slg", "kode_tech", "datek", "rk_name", "induk_gamas", "reported_date", "ttr_customer", "ttr_nasional",
            "ttr_regional", "ttr_witel", "ttr_mitra", "ttr_agent", "status", "hasil_ukur", "osm_resolved", "last_update_tic", "status_date", "closed_time", "resolved_by",
            "kode_workzone", "witel", "regional", "incident_sym", "solution_segment", "actual_solution", "compliance", "exclude", "gamas", "week", "kode_problem", "kode_groupact", "flag");

        foreach ($arr_data as $row):
            if ($this->cekIsiData($row, $baris) == true) :
                $kode = intval($kl) . $urutankode;
                /*
                 * $x := nilai untuk array $kolom
                 * $k := nilai untuk array xls
                 */
                $x = 0;
                $k = 1;
                $i = 1;
                $data = array();
                $data2 = array();
                foreach ($row as $col):
                    if (substr($col, 0, 1) == "=") {
                        $nilai = "";
                    } else {
                        $nilai = ($col == '') ? NULL : $col;
                    }
                    switch ($x):
                        case 6:
                        case 8:
                        case 26:
                        case 36:
                        case 37:
                            $UNIX_DATE = ($nilai - 25569) * 86400;
                            if ($UNIX_DATE < 0) {
                                $data[$kolom[$x]] = $this->libglobal->dateReverseFromText($nilai);
                            } else {
                                $data[$kolom[$x]] = ($nilai == '') ? NULL : gmdate("Y-m-d", $UNIX_DATE);
                            }
                            break;
                        case 0:
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM t_" . $type . "_open WHERE kode ='$nilai' LIMIT 1", 1);
                            if (empty($carikode)) {
                                $data['kode'] = $nilai;
                                $data['flag'] = '0';
                            } else {
                                $data['flag'] = '1';
                                $data['kode'] = $nilai;
                            }
                            break;
                        case 22:
                            $pisah = explode(' ', $nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_technology WHERE nama_tech LIKE '%$pisah[0]%' LIMIT 1", 1);
                            $data['kode_tech'] = ($carikode) ? $carikode->kode : 0;
                            break;
                        case 40:
                            $pisah = explode(' ', $nilai);
                            $carikode = $this->Data_model->jalankanQuery("SELECT * FROM m_workzone WHERE nama_workzone LIKE '%$pisah[0]%' LIMIT 1", 1);
                            $data['kode_workzone'] = ($carikode) ? $carikode->kode : 0;
                            break;
                        // case 41:
                        //     $setring = str_replace( , "", $nilai);
                        //     if ($setring == "BANDUNGBARAT") {
                        //         $witel = 1;
                        //     } else if ($setring == "JABARSELATAN(SUKABUMI)") {
                        //         $witel = 2;
                        //     } else if ($setring == "JABARTENGAH(BANDUNG)") {
                        //         $witel = 3;
                        //     } else if ($setring == "JABARTIMSEL(TASIKMALAYA)") {
                        //         $witel = 4;
                        //     } else if ($setring == "JABARTIMUR(CIREBON)") {
                        //         $witel = 5;
                        //     } else if ($setring == "JABARUTARA(KARAWANG)") {
                        //         $witel = 6;
                        //     } else {
                        //         $witel = "KOSONG";
                        //     }
                        //     if ($type == "datin") {
                        //         $q = $this->Data_model->jalankanQuery("SELECT kode, service_id, count(*) as countdata FROM t_datin WHERE service_id ='" . $data['service_id'] . "'", 3);
                        //         $a = $this->Data_model->jalankanQuery("SELECT kode FROM t_datin WHERE service_id ='" . $data['service_id'] . "'", 3);
                        //     } else {
                        //         $q = $this->Data_model->jalankanQuery("SELECT kode, service_id, count(*) as countdata FROM t_pots WHERE service_id ='" . $data['service_id'] . "'", 3);
                        //         $a = $this->Data_model->jalankanQuery("SELECT kode FROM t_pots WHERE service_id ='" . $data['service_id'] . "'", 3);
                        //     }

                        //     if ($q[0]->countdata == 0) {
                        //         if (empty($koderef)) {
                        //             $koderef = "Tidak Ada";
                        //         } else {
                        //             $koderef = $a[0];
                        //         }
                        //         $totaldata = $q[0]->countdata + 1;
                        //         $data2 = array(
                        //             'notik' => $data['kode'],
                        //             'service_id' => $data['service_id'],
                        //             'kode_witel' => $witel
                        //         );

                        //         $text = "No. Tiket = " . $data['kode'] . "\nService ID = " . $data['service_id'] . "\nGangguan Ke-" . $totaldata;
                        //         $text .= "\nTiket Referensi : " . $koderef;
                        //         $text .= "\n\nMohon Perhatian Dan Pengawalannya";
                        //         $this->BroadcastGaul($totaldata, $data['kode'], $data['service_id'], $witel, $text);
                        //     } else {
                        //         $totalgaul = $q[0]->countdata + 1;
                        //         $data2 = array(
                        //             'notik' => $data['kode'],
                        //             'service_id' => $data['service_id'],
                        //             'kode_witel' => $witel
                        //         );

                        //         $text = "No. Tiket = " . $data['kode'] . "\nService ID = " . $data['service_id'] . "\nGangguan Ke-" . $totalgaul;
                        //         $text .= "\nTiket Referensi : \n";
                        //         for ($s = 0; $s < count($a); $s++) {
                        //             $koderef = $a[$s];
                        //             $text .= $koderef->kode . "\n";
                        //         }
                        //         $text .= "\nMohon Perhatian Dan Pengawalannya";
                        //         $this->BroadcastGaul($totalgaul, $data['kode'], $data['service_id'], $witel, $text);
                        //     }
                        //     break;
                        default :
                            $data[$kolom[$x]] = $nilai;
                            break;
                    endswitch;
                    $k++;
                    $x++;

                endforeach;
                $i++;
                $data['week'] = $this->input->post('week');
                try {
                    if ($data['flag'] == '1') {
                        $kondisi = array('kode' => $data['kode']);
                        $flag = array('flag' => $data['flag']);
                        $this->Data_model->updateDataWhere($data, "t_" . $type . "_open", $kondisi);
                        $this->cekDataClose2($data['service_id'], $data['kode'], $data['witel'], $type);
                    } else {
                        
                        $this->Data_model->simpanData($data, "t_" . $type . "_open");
                        $this->cekDataClose2($data['service_id'], $data['kode'], $data['witel'], $type);
                    }
                } catch (Exception $exc) {
                    $data['infoerror'] = $exc->getMessage();
                }

            else:

            endif;

        endforeach;
    }

    function cekIsiData($row, $baris) {
        $isi = false;
        foreach ($row as $eusi) {
            $isi = (strlen(trim($eusi)) > 0) ? true : (($isi == true) ? $isi : false);
        }
        return $isi;
    }

}
