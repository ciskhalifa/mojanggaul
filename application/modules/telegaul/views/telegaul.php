<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>Check Tiket Gaul</b></a>
    </div>
    <div class="register-box-body">
        <form role="form" id="sendFormCheck2" enctype="multipart/form-data"  class="form-horizontal">
            <div class="form-group">
                <label class="col-md-4 label-control" for="exampleInputFile">Type</label>
                <input type="radio" name="type" value="datin" required>&nbsp;&nbsp;DATIN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;              
                <input type="radio" name="type" value="pots">&nbsp;&nbsp; POTS
            </div>
            <div class="form-group">
                <label class="col-md-4 label-control" for="exampleInputFile">No. Tiket</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" name="notik" id="notik" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 label-control" for="exampleInputFile">Service ID</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" name="service_id" id="service_id" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 label-control" for="exampleInputFile">Witel</label>
                <div class="col-md-12">
                    <select name="kode_witel" class="form-control select2" required>
                        <option value="">- Pilihan -</option>
                        <?php
                        $q = $this->Data_model->selectData("m_witel", "kode");
                        foreach ($q as $row):
                            ?>
                            <option value="<?= $row->kode; ?>"><?= $row->nama_witel; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <textarea name="textmen" id="textmen" hidden=""></textarea>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary" id="tescheck">Check</button>
                </div>
                <div class="col-xs-8">
                    <button type="button" class="btn btn-primary pull-right" id="importTiketOpen"><i class="fa fa-upload"></i></button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.form-box -->
</div>