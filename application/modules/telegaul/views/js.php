<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<div class="modal fade text-xs-left" id="modalImportTiketOpen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form  role="form" id="sendForm" enctype="multipart/form-data"  class="form-horizontal">
                <div class="modal-header">
                    <h4 class="modal-title">Import Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">Type</label>
                        <div class="col-md-4">
                            <input type="radio" name="type" value="datin" required>&nbsp;&nbsp;DATIN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;              
                            <input type="radio" name="type" value="pots">&nbsp;&nbsp; POTS
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">Week</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="week">
                        </div>
                    </div>
                    <textarea name="textmen" id="textmen2" hidden=""></textarea>
                    <div class="form-group">
                        <label class="col-md-2 label-control" for="exampleInputFile">File</label>
                        <div class="col-md-4">
                            <input type="file" name="file" id="file">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?= base_url('assets/jquery.isloading.min.js'); ?>"></script>
<!-- iCheck -->
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js') ?>"></script>
<script>
    $(function () {
        $('#importTiketOpen').on('click', function () {
            $('#modalImportTiketOpen').modal();
        });
        $("#file").on("change", function () {
            var file = this.files[0];
            var fileName = file.name;
            var fileType = file.type;
            console.log(fileType);
            if (fileType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || fileType == "application/vnd.ms-excel") {
                $('#label').text(fileName);
                $('#reset').removeClass('hidden');
            } else {
                alert("Maaf format dokumen tidak sesuai");
                $(this).val('');
                $('#label').text('Pilih File');
            }
        });
        $('#sendForm').on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'telegaul/importExcel/';
                var data = new FormData(this);
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $("body").isLoading({
                            text: "",
                            position: "overlay"
                        });
                    },
                    success: function (html) {
                        $("body").isLoading("hide");
                    },
                    error: function () {
                        alert(html);
                        $("body").isLoading("hide");
                    },
                });
                return false;
            }
        });
        $('#sendFormCheck2').on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'telegaul/cekDataClose/';
                var data = $("#sendFormCheck2").serialize();
                ;
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    beforeSend: function (data) {
                        $("body").isLoading({
                            text: "",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                        });

                    },
                    success: function (html) {
                        check(html);
                       

                    },
                    error: function () {
                        alert(html);
                        $("body").isLoading("hide");
                    },
                });
                return false;
            }
        });
    });
    function check(html) {
        var txt;
        $('#textmen').val(html);
        var r = confirm(html);
        if (r == true) {
            $.ajax({
                sync: true,
                url: "telegaul/kirimData",
                data: $("#sendFormCheck2").serialize(),
                type: 'POST',
                datatype: 'html',
                beforeSend: function (data) {
                    $("body").isLoading({
                        text: "",
                        position: "overlay",
                        tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                    });
                },
                success: function (html) {
                    alert(html);
                    $("body").isLoading("hide");
                },
                error: function () {
                    alert(html);
                    $("body").isLoading("hide");
                },
            });
            $("body").isLoading("hide");
        } else {
            $("body").isLoading("hide");
        }
    }
</script>