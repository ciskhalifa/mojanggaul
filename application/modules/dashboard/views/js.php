<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<script src="<?= base_url('assets/jquery.isloading.min.js'); ?>"></script> 
<script src="<?= base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script>
    var myApp = myApp || {};
    $(function () {
        var kode = $("#kode_perusahaan").val();
        $("#data-detail").dataTable();
        getPing();
        $(".data-monitor").each(function (i, obj) {
            myApp[monitor] = {};
            var monitor = $(obj).attr("data-id");
            myTableCIS("#monitor-" + monitor,
                    {
                        ajaxSource: "dashboard/listData/" + monitor,
                        columnDefs: [
                            {targets: [0], sortable: false, sWidtht: "35px"},
                            {targets: [2], sortable: false, sWidth: "268px"},
                            {targets: [4], sWidth: "268px"},
                            {targets: [0], class: "text-center"},
                            {targets: [3], render: function (data, type, row) {
                                    var display = '';
                                    var status = piceunSpasi(data);
                                    if (status == "Requesttimedout") {
                                        display = '<button type="button" class="btn btn-danger btn-circle"><i class="fa fa-power-off"></i></button>';
                                    } else {
                                        display = '<button type="button" class="btn btn-success btn-circle"><i class="fa fa-check"></i></button>';
                                    }
                                    return display;
                                }},
                            {targets: [5], render: function (data, type, row) {
                                    var display = '';
                                    var kode = piceunSpasi(data);
                                    display += ' <button class="btn btn-info" data-kode="' + kode + '" data-bind="detail" data-tabel="dashboard/detail"><i class="fa fa-search"></i></button>';
                                    return display;
                                }, sortable: false, sClass: "center p-r-0", sWidth: "125px"}
                        ]
                    }
            , monitor);
        });

        function getPing() {
            myApp[monitor] = {};
            var monitor = $(".data-monitor").attr("data-id");
            var interval = setInterval(function () {
                $.ajax({
                    url: "dashboard/getPing",
                    success: function (data) {
                        location.reload(true);
                    }
                });
            }, 60000);
        }
    });
</script>

