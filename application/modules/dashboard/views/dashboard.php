<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<input type="hidden" id="tabel" value="<?php echo $tabel; ?>">
<input type="hidden" id="kolom" value="<?php echo $jmlkolom; ?>">
<!-- Main content -->
<section class="content">
    <div class="row">
        <?php
        foreach ($perusahaan as $p):
            $q = $this->Data_model->jalankanQuery("SELECT * FROM vdash WHERE kode_perusahaan=" . $p->kode_perusahaan, 3);
            ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box" style="background-color: <?= $q[0]->style; ?>">
                    <div class="inner">
                        <h3 style="color:white;"><?= $p->nama_perusahaan; ?></h3>
                        <p style="color:white;">Total Jaringan : <?= $q[0]->countdata; ?></p>
                        <p style="color:white;">Total UP       : <?= $q[0]->connect; ?></p>
                        <p style="color:white;">Total Down     : <?= $q[0]->rto; ?></p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-building"></i>
                    </div>
                    <a href="<?= base_url("dashboard/view_detail/" . $q[0]->kode_perusahaan); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>


