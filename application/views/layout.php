<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<!DOCTYPE html>
<html lang="en" data-textdirection="LTR" class="loading">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="author" content="Fahrizal Nuansa">
        <title>MONITORING JARINGAN ROC 3</title>
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>asset/app-assets/images/ico/apple-icon-60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>asset/app-assets/images/ico/apple-icon-76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>asset/app-assets/images/ico/apple-icon-120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>asset/app-assets/images/ico/apple-icon-152.png">
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>asset/app-assets/images/ico/favicon.ico">
        <link rel="shortcut icon" type="image/png" href="<?= base_url() ?>asset/app-assets/images/ico/favicon-32.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/bootstrap.min.css">
        <!-- font icons-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/fonts/icomoon.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/vendors/css/sliders/slick/slick.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/vendors/css/extensions/pace.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/vendors/css/charts/morris.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/vendors/css/extensions/unslider.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/vendors/css/weather-icons/climacons.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/vendors/css/file-uploaders/dropzone.min.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN ROBUST CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/app.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/assets/css/style.css">
        <!-- END ROBUST CSS-->
        <!-- BEGIN Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/core/menu/menu-types/horizontal-menu.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/core/menu/menu-types/vertical-overlay-menu.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/core/colors/palette-gradient.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/plugins/calendars/clndr.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/css/plugins/file-uploaders/dropzone.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
        <!-- END Page Level CSS-->
        <!-- SELECT 2 CSS -->
        <link rel="stylesheet" type="text/css" href="<?= base_url('asset/app-assets/vendors/css/forms/selects/select2.min.css'); ?>">
        <!-- BEGIN Custom CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>asset/assets/css/style.css">
        <!-- END Custom CSS-->
        <style type="text/css">
            .table td, .table th {
                padding: .75rem;
            }

            .table thead th {
                vertical-align: middle;
                text-align: center;
                border-bottom: 1px solid #ECEEEF;
            }

            .table-responsive {
                font-size: 0.9rem;
            }

            .dataTables_filter{
                margin-top: -16px;
            }
        </style>
        <?php ($css != '') ? $this->load->view($css) : ''; ?> 
    </head>
    <body data-open="hover" data-menu="horizontal-top-icon-menu" data-col="2-columns" class="horizontal-layout horizontal-top-icon-menu 2-columns ">

        <!-- navbar-fixed-top--> 
        <nav class="header-navbar navbar navbar-with-menu undefined navbar-light navbar-border navbar-brand-center">
            <div class="navbar-wrapper">
                <div class="navbar-header">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5 font-large-1"></i></a></li>
                        <!--<li class="nav-item"><a href="<?= base_url() ?>" class="navbar-brand nav-link"><img alt="branding logo" src="<?= base_url() ?>asset/app-assets/images/logo/logo1.png" data-expand="<?= base_url() ?>asset/app-assets/images/logo/logo1.png" data-collapse="<?= base_url() ?>asset/app-assets/images/logo/robust-logo-small.png" class="brand-logo" style="width: 200px;margin-top: -7px;"></a></li>-->
                        <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="icon-ellipsis pe-2x icon-icon-rotate-right-right"></i></a></li>
                    </ul>
                </div>
                <div class="navbar-container container center-layout" style="width: 100%;">
                    <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                        <ul class="nav navbar-nav">
                            <li class="nav-item hidden-sm-down"><a class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="icon-menu5"></i></a></li>
                            <li class="nav-item hidden-sm-down"><a href="#" class="nav-link nav-link-expand"><i class="ficon icon-expand2"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav float-xs-right">
                            <li class="dropdown dropdown-user nav-item">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link"><span class="avatar avatar-online"><img src="<?= base_url() ?>asset/app-assets/images/portrait/small/avatar-s-1.png" alt="avatar"><i></i></span><span class="user-name"><?= ($_SESSION['username'] != "") ? $_SESSION['nama'] : ""; ?></span></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a href="<?= base_url('login/doOut'); ?>" class="dropdown-item"><i class="icon-power3"></i> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- Horizontal navigation-->
        <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-bordered navbar-shadow">
            <!-- Horizontal menu content-->
            <div data-menu="menu-container" class="navbar-container main-menu-content container center-layout" style="width: 100%;">
              <!-- include <?= base_url() ?>asset/includes/mixins-->
                <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
                    <li class="nav-item"><a href="<?= base_url() ?>dashboard" class="nav-link"><i class="icon-home3"></i><span data-i18n="nav.dash.main">Dashboard</span></a>
                    </li>
                    <li class="nav-item"><a href="<?= base_url() ?>master" class="nav-link"><i class="icon-office"></i><span data-i18n="nav.dash.main">Master Data</span></a>
                    </li>
                </ul>
            </div>
            <!-- /horizontal menu content-->
        </div>
        <!-- Horizontal navigation-->

        <div class="app-content content container-fluid">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body"><!-- Sales stats -->
                    <?php $this->load->view($content); ?>
                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->

        <!-- style="position: absolute;right: 0;bottom: 0;left: 0;" -->
        <footer class="navbar footer navbar-fixed-bottom footer-light footer-shadow container-fluid">
            <p class="clearfix text-muted text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Copyright  &copy; <?= date('Y') ?> <a href="#" target="_blank" class="text-bold-800 grey darken-2">REGIONAL 3 </a>, All rights reserved. </span><span class="float-md-right d-xs-block d-md-inline-block">Last Update : <?= date("Y/m/d H:i:s"); ?></span></p>
        </footer>
        
        <!-- BEGIN VENDOR JS-->
        <!-- build:js app-assets/js/vendors.min.js-->
        <script src="<?= base_url() ?>asset/app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/ui/blockUI.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/ui/jquery.matchHeight-min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/sliders/slick/slick.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/ui/screenfull.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/extensions/pace.min.js" type="text/javascript"></script>
        <!-- /build-->
        <!-- BEGIN VENDOR JS-->
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/forms/select/select2.full.min.js" type="text/javascript"></script>
        <!-- BEGIN PAGE VENDOR JS-->
        <script type="text/javascript" src="<?= base_url() ?>asset/app-assets/vendors/js/ui/jquery.sticky.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>asset/app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/extensions/moment.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/extensions/underscore-min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/extensions/clndr.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/charts/echarts/echarts.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
        <!-- END PAGE VENDOR JS-->
        <!-- BEGIN ROBUST JS-->
        <!-- build:js app-assets/js/app.min.js-->
        <script src="<?= base_url() ?>asset/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>asset/app-assets/js/core/app.min.js" type="text/javascript"></script>
        <!-- /build-->
        <!-- END ROBUST JS-->
        <!-- BEGIN PAGE LEVEL JS-->
        <script type="text/javascript" src="<?= base_url() ?>asset/app-assets/js/scripts/ui/breadcrumbs-with-stats.min.js"></script>
        <!-- END PAGE LEVEL JS-->
        <script src="<?= base_url() ?>asset/js/functions.js" type="text/javascript"></script>        
        <script src="<?= base_url() ?>asset/js/formValidation.min.js" type="text/javascript"></script>
        <?php ($js != '') ? $this->load->view($js) : ''; ?>
        <!-- MY JS -->


    </body>

</html>