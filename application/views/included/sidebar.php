<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="<?= base_url('dashboard'); ?>">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="<?= base_url('master'); ?>" class="">
                    <i class="fa fa-database"></i>
                    <span>Master Data</span>
                </a>
            </li>
            <li>
                <a href="<?= base_url('datin'); ?>" class="">
                    <i class="fa fa-database"></i>
                    <span>Upload Data DATIN</span>
                </a>
            </li>
            <li>
                <a href="<?= base_url('pots'); ?>" class="">
                    <i class="fa fa-database"></i>
                    <span>Upload Data POTS</span>
                </a>
            </li>
<!--            <li class="treeview">
                <a href="<?= base_url('datin'); ?>">
                    <i class="fa fa-cloud-upload"></i>
                    <span>Data Gaul</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview"><a href="#"><i class="fa fa-circle-o"></i> DATIN
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu" style="display: none;">
                            <li><a href="<?= base_url('datin/open'); ?>"><i class="fa fa-circle-o"></i> Tiket Open</a></li>
                            <li><a href="<?= base_url('datin/close'); ?>"><i class="fa fa-circle-o"></i> Tiket Close</a></li>
                        </ul>
                    </li>
                    <li><a href="<?= base_url('pots') ?>"><i class="fa fa-circle-o"></i> POTS</a></li>
                </ul>
            </li>-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>