<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<?php

$data['js'] = $js;
$data['css'] = $css;

$this->load->view(VIEW_INCLUDE . '/headercek', $data);
$this->load->view($content);
$this->load->view(VIEW_INCLUDE . '/footercek', $data);
?>