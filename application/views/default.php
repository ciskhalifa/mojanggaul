<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<?php

    $data['js'] = $js;
    $data['css'] = $css;

    $this->load->view(VIEW_INCLUDE . '/header', $data);
    $this->load->view(VIEW_INCLUDE . '/sidebar');
     echo '<div class="content-wrapper">';
        $this->load->view($content);
     echo '</div>';
    $this->load->view(VIEW_INCLUDE . '/footer', $data);
?>