<?php 
/* Name     : Christiantinus Nesi
 * Email    : christiantinusnesi@gmail.com
 * Created By : Christiantinus Nesi
 */
?>
<!-- jQuery 3 -->
<script src="<?= base_url('assets/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- iCheck -->
<script src="<?= base_url('assets/plugins/iCheck/icheck.min.js') ?>"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(function () {
        $('#sendFormCheck').on('submit', function (e) {
            if (e.isDefaultPrevented()) {
                // handle the invalid form...
            } else {
                var link = 'telegaul/cekDataClose/';
                var data = $("#sendFormCheck").serialize();
                ;
                $.ajax({
                    sync: true,
                    url: link,
                    data: data,
                    type: 'POST',
                    datatype: 'html',
                    beforeSend: function () {
                        $("body").isLoading({
                            text: "",
                            position: "overlay",
                            tpl: '<span class="isloading-wrapper %wrapper%" style="background:none;">%text%<div class="preloader pls-amber" style="position: absolute; top: 0px; left: -40px;"><svg class="pl-circular" viewBox="25 25 50 50"><circle class="plc-path" cx="50" cy="50" r="20"/></svg></div>'
                        });
                    },
                    success: function (html) {
                        alert(html);
                        $("body").isLoading("hide");

                    },
                    error: function () {
                        alert(html);
                        $("body").isLoading("hide");
                    },
                });
                return false;
            }
        });
    });
</script>
</body>
</html>